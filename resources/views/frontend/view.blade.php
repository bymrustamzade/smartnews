@extends('frontend.app')

@section('title', $data['title'])
@section('meta_title', $data['seo_title'])
@section('meta_datecreated', $data['created_at'])
@section('meta_author', env('APP_NAME'))
@section('meta_headline', $data['title'])
@section('meta_description', $data['seo_description'])
@section('meta_keywords', $data['seo_keywords'])
@section('meta_image', 'https://www.smartnews.az' . $data['main_img'])
@section('meta_robots', $data['seo_robots'])

@section('middle')
<div class="uk-width-expand rm-view-height rm-padding-55" id="loadmore">
	<div uk-grid>
		<div class="uk-width-1-1" style="padding-left: 0;">
			<picture>
				<source srcset="{{$data['main_img']}}" media="min-width: 641px">
				<source srcset="{{$data['img_path'] . '300/' . $data['img_filename']}}" media="max-width: 640px">
				<img src="{{$data['main_img']}}" class="rm-view-img rm-border-top-rounded">
			</picture>
		</div>
		<div class="uk-width-1-1 rm-content-background uk-border-rounded" style="padding-left: 10px;  margin-top: 20px !important;">
			<div class="uk-width-1-1" style="padding: 10px; margin: 0;" uk-grid>
				<div class="uk-width-1-2" style="padding: 0px;">
					<span class="uk-badge rm-badge-color" style="border-radius: 5px !important;">
						{{$data['created_at']}}
					</span>
				</div>
				<div class="uk-width-1-2" style="padding: 0px; text-align: right;">
					@foreach($data['topics'] as $topic)
						<a href="/p/{{$topic}}" style="text-decoration: none;">
							<span class="uk-badge rm-topic-badge" style="border-radius: 5px !important;">
								{{$topic}}
							</span>
						</a>
					@endforeach
				</div>
			</div>
			<div class="uk-width-1-1" style="padding: 10px;">
				<h2>{{$data['title']}}</h2>
				<p class="uk-text-muted">{{$data['description']}}</p>
			</div>
		</div>
		@if(!empty($data['images']))
			<div class="uk-width-1-1" style="padding-left: 0;  margin-top: 20px !important; text-align: center;">
				@foreach($data['images'] as $image)
					<img src="{{$image}}" class="rm-view-images uk-border-rounded">
				@endforeach
			</div>
		@endif
		<div class="uk-width-1-1 rm-content-background uk-border-rounded" style="padding: 20px;  margin-top: 20px !important;">
			{!! $data['body'] !!}
			<div class="addthis_inline_share_toolbox"></div>
		</div>
	</div>
</div>

@endsection