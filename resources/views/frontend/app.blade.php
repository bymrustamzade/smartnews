<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">

    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <meta name='apple-mobile-web-app-capable' content='yes' />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('/storage/config/logo.ico')}}">

    <meta name="description" content="@yield('meta_description')">

    <meta itemprop="dateCreated" content="@yield('meta_datecreated')">
    <meta itemprop="url" content="{{url()->current()}}">
    <meta itemprop="author" content="@yield('meta_author')">
    <meta itemprop="headline" content="@yield('meta_headline')">
    <meta itemprop="description" content="@yield('meta_description')">
    <meta itemprop="keywords" content="@yield('meta_keywords')">
    <meta itemprop="image" content="@yield('meta_image')">

	<meta name="keywords" content="@yield('meta_keywords')">
	<meta name="robots" content="@yield('meta_robots')">

	<meta property="og:title" content="@yield('meta_title')">
	<meta property="og:description" content="@yield('meta_description')">
	<meta property="og:image" content="@yield('meta_image')">
	<meta property="og:url" content="{{url()->current()}}" />

	<meta name="twitter:title" content="@yield('meta_title')">
	<meta name="twitter:description" content="@yield('meta_description')">
	<meta name="twitter:image" content="@yield('meta_image')">

    <!-- Styles -->
    <link href="{{ asset('frontend/css/uikit.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/app.css') }}" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119397512-2"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-119397512-2');
	</script>
</head>
<body>
	<div>
		@section('categories')
			@foreach($categories as $categorie)
		    	<li>
		    		<a class="rm-category-url @if(Route::input('id_type') && Route::input('id_type') == $categorie['key']) rm-slected-type @endif" href="/t/{{$categorie['key']}}">{{$categorie['text']}}</a>
		    	</li>
		    @endforeach
	    @endsection
	    
		@component('layouts.header')@endcomponent
		
		<div class="rm-content">
			<div class="uk-container uk-container-large" style="max-width: 1100px !important; padding: 0;">
				<div style="margin-left: 0 !important;" uk-grid>
					<div class="uk-width-1-6@l uk-width-1-5@m uk-width-1-4@s uk-visible@m" style="padding: 0; padding-left: 10px;">
		    			@component('layouts.left')@endcomponent
					</div>
					<div class="uk-width-expand uk-width-1-1@m" style="margin: 0 !important; padding: 0;" uk-grid>
						<div class="uk-width-3-4@m rm-middle-padding">
							@yield('middle')
						</div>
						<div class="uk-width-1-4@m uk-visible@m" style="padding-left: 0; padding-right: 10px;">
							@component('layouts.right')@endcomponent
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <script src="{{ asset('frontend/js/uikit.min.js') }}"></script>
    <script src="{{ asset('frontend/js/uikit-icons.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/js/platform.js') }}"></script>
    <script src="{{ asset('frontend/js/app.js') }}"></script>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5e5ce52bbbc478e3"></script>
</body>
</html>
