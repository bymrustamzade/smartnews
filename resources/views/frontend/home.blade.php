@extends('frontend.app')

@section('title', env('APP_NAME'))
@section('meta_title', env('APP_NAME'))
@section('meta_description', env('APP_NAME'))

@section('middle')
<h4 class="uk-margin-small-top" style="margin-bottom: 5px;">
	<b class="uk-margin-left rm-out-header">{{__('news_wall')}}</b>
	<span class="uk-margin-small-right rm-to-top" uk-icon="arrow-up" style="float: right; font-size: 14px;">{{__('to_top')}}</span>
</h4>
<div class="uk-width-expand rm-content-height rm-padding-55" id="loadmore">
	<div id="fill-loadmore" uk-grid>
		<input type="hidden" id="ajaxUrl" value="/loadmore-home?">
		<meta name="format-detection" content="telephone=no">
		@include('layouts.content')
	</div>
</div>
@endsection