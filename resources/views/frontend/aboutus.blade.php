@extends('frontend.app')

@section('meta_title', 'Welcome to smartnews')

@section('content')
<div style="width: 100%;">
	<div class="uk-container uk-container-large uk-margin-top">
		<div class="rmdrkmd">
			@if(!empty($settings))
				{!! $settings->about_us !!}
			@endif
		</div>
	</div>
</div>
@endsection