@extends('frontend.app')

@section('meta_title', 'Welcome to smartnews')

@section('content')
<div class="uk-container uk-container-large uk-margin-top" v-if="!empty_data">
	<div uk-grid>
		<div class="uk-width-3-4@m">
			<h4 class="uk-heading-small rm-darkmode-color-white">{{__('tv-schedule')}}</h4>
			<div class="uk-grid-divider uk-child-width-expand@m" uk-grid>
				@foreach($data as $item)
					<div class="uk-width-1-3 rmdrkmd">
						<p class="uk-text-emphasis">
							{{$item['day']}}
						</p>
						<hr class="uk-divider-small">


						@foreach($item['data'] as $hour)
							<p class="uk-text-muted uk-margin-small">
								<b>{{$hour->hour_start}}:</b> {{$hour->description}}
							</p>
						@endforeach

					</div>
				@endforeach
			</div>
		</div>
		<div class="uk-width-1-4@m uk-visible@m">
			<h4 class="uk-heading-small rm-darkmode-color-white">{{__('news')}}</h4>
			@component('layouts.right')@endcomponent
		</div>
	</div>
</div>
@endsection