@extends('frontend.app')

@section('title', env('APP_NAME'))
@section('meta_title', env('APP_NAME'))
@section('meta_description', env('APP_NAME'))

@section('middle')
<h4 class="uk-margin-small-top">
	<b class="uk-margin-left rm-out-header">{{__('news_wall')}}</b>
	<span class="uk-margin-small-right rm-to-top" uk-icon="arrow-up" style="float: right; font-size: 14px;">{{__('to_top')}}</span>
</h4>
<div class="uk-width-expand rm-content-height rm-padding-55" id="loadmore">
	<div id="fill-loadmore" uk-grid>
		<input type="hidden" id="ajaxUrl" value="/loadmore-type/{{Route::input('id_type')}}?">
		@if(!empty($data['content']))
			@include('layouts.content')
		@else
			@include('layouts.empty')
		@endif
	</div>
</div>
@endsection