<?php echo '<?xml version="1.0" encoding="UTF-8"?>';?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9" xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0" xmlns:pagemap="http://www.google.com/schemas/sitemap-pagemap/1.0" xmlns:xhtml="http://www.w3.org/1999/xhtml"> 
	<url>
		<loc>https://www.smartnews.az/</loc>
		<lastmod>2020-03-03T09:13:21+04:00</lastmod>
		<priority>1.00</priority>
	</url>
	@foreach($types as $type)
		<url>
			<loc>https://www.smartnews.az/t/{{$type->id_type}}</loc>
			<lastmod>{{$type->created_at->toRfc3339String()}}</lastmod>
			<priority>0.90</priority>
		</url>
	@endforeach
	@foreach($contentdata as $data)
		<url> 
			<loc>https://www.smartnews.az/v/{{$data->slug}}</loc> 
			<news:news> 
				<news:publication> 
					<news:name>{{$data->title}}</news:name> 
					<news:language>{{$data->language}}</news:language>
				</news:publication>
				<news:publication_date>{{$data->created_at->toRfc3339String()}}</news:publication_date> 
				<news:title>{{$data->title}}</news:title> 
			</news:news> 
		</url>
	@endforeach
</urlset>