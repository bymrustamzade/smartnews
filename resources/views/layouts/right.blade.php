<div class="done" id="rm-move-web">
	<div class="mover">
		<h4 class="uk-margin-small-top"><b class="rm-out-header">{{__('widgets')}}</b></h4>
		<div class="uk-border-rounded uk-width-expand rm-side-scroll rm-content-background">
			<ul class="rm-right-list">
				@foreach($content_list as $item)
					<li>
						<article class="rm-right-article">
							<h3 class="rm-list-head"><a class="rm-right-list-url" href="{{$item['url']}}">{{$item['title']}}</a></h3>
							<span class="rm-content-time-small">{{date('H:i d-m-Y', $item['created_at'])}}</span>
						</article>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>