<div class="uk-border-rounded rm-side-scroll">
	<h4 class="uk-margin-small-top"><b class="uk-margin-left rm-out-header">{{__('categories')}}</b></h4>
	<ul class="uk-list uk-margin-left">
		@yield('categories')
	</ul>
</div>