@foreach($data['content'] as $index => $item)
	@if($index == 0 || $item['is_paid'] == true || $item['is_big'] == 1)
		<div class="uk-width-1-1 rm-content-background uk-border-rounded" style="padding-left: 0;">
			<a href="{{$item['url']}}">
				@if(array_key_exists('img', $item))
					<picture>
						<source srcset="{{$item['img']}}" media="min-width: 641px">
						<source srcset="{{$item['img_path'] . '300/' . $item['img_filename']}}" media="max-width: 640px">
						<img src="{{$item['img']}}" class="rm-content-img rm-border-top-rounded">
					</picture>
				@else
					<img src="{{asset('/storage/config/sn-placeholder.jpg')}}" class="rm-content-img rm-border-top-rounded">
				@endif
			</a>
			<div class="uk-width-1-1" style="padding: 10px; margin: 0;" uk-grid>
				<div class="uk-width-1-3@s" style="padding: 0px;">
					<span class="uk-label rm-badge-color">
						{{date('H:i d-m-Y', $item['created_at'])}}
					</span>
					@if($item['is_paid'])
						<span class="uk-label rm-badge-color">
							{{__('paid')}}
						</span>
					@endif
				</div>
				<div class="uk-width-2-3 uk-visible@s" style="padding: 0px; text-align: right;">
					<div class="addthis_inline_share_toolbox" data-url='smartnews.az{{$item['url']}}'></div>
				</div>
			</div>
			<div class="uk-width-1-1" style="padding: 10px;">
				<h3>
					<a class="rm-content-header" href="{{$item['url']}}">{{$item['title']}}</a>
				</h3>
				<a href="{{$item['url']}}" class="uk-text-muted rm-link-hover" style="text-decoration: none;">
					<p>{{$item['description']}}</p>
				</a>
			</div>
		</div>
	@else
		<div class="uk-width-1-1 rm-content-background uk-border-rounded" style="padding-left: 0; position: relative; display: table;">
			<div class="uk-panel">
				<a href="{{$item['url']}}">
					@if(array_key_exists('img', $item))
						<picture>
							<source srcset="{{$item['img']}}" media="min-width: 641px">
							<source srcset="{{$item['img_path'] . '300/' . $item['img_filename']}}" media="max-width: 640px">
							<img src="{{$item['img']}}" class="rm-content-img-small rm-border-left-rounded" data-resize="300:*;">
						</picture>
					@else
						<img src="{{asset('/storage/config/sn-placeholder.jpg')}}" class="rm-content-img-small rm-border-left-rounded">
					@endif
				</a>
				<div class="uk-expand" style="padding-right: 10px;">
					<h4 style="margin-bottom: 5px;">
						<a class="rm-content-header" href="{{$item['url']}}">{{$item['title']}}</a>
					</h4>
					<span class="rm-content-time-small">{{date('H:i d-m-Y', $item['created_at'])}}</span>
					<p class="uk-text-muted" style="margin-top: 0; margin-bottom: 5px; padding-left: 5px; font-size: 13px;">{{$item['description']}}</p>
				</div>
			</div>
		</div>
	@endif
@endforeach