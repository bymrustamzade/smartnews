<nav class="rm-navbar uk-navbar-container uk-margin uk-visible@m" uk-navbar>
    <div class="uk-navbar-left uk-width-auto">
        <a class="rm-logo uk-navbar-item uk-logo uk-margin-left uk-margin-right" href="/">smart<b>news</b></a>
    </div>
    <div class="uk-navbar-right uk-width-expand">
    	<div class="uk-visible@m">
    		<ul class="uk-navbar-nav uk-margin-left">
	            <li class="rm-navbar-list">
	                <a class="rm-navbar-link" href="/">
	                    <span class="uk-icon uk-margin-small-right" uk-icon="icon: home"></span>
	                    {{__('home')}}
	                </a>
	            </li>
	            {{-- <li class="rm-navbar-list">
	                <a class="rm-navbar-link" href="#">
	                    <span class="uk-icon uk-margin-small-right" uk-icon="icon: play"></span>
	                    {{__('broadcast')}}
	                </a>
	            </li> --}}
	        </ul>
	    	<div class="uk-navbar-item uk-width-1-4 uk-position-right">
	            <form id="nav-search-form" class="uk-search uk-search-default uk-width-1-1" action="/search" method="GET" role="search">
			        <a href="" id="search-button" class="uk-search-icon-flip" uk-search-icon></a>
			        <input class="uk-search-input uk-border-rounded rm-search-input" type="search" placeholder="{{__('search_text')}}" name="q" style="background-color: #f7f8fa; border: 1px solid #e5e5e529;">
			    </form>
	        </div>
    	</div>
    </div>
</nav>
<div class="rm-nav-item-dropped uk-hidden@m" id="rm-mobile-nav-list-toggle">
	<h4 class="uk-margin-small-top"><b class="uk-margin-left rm-out-header">{{__('categories')}}</b></h4>
    <ul class="uk-list uk-margin-left">
		@yield('categories')
	</ul>
</div>
<div class="rm-nav-item-dropped uk-hidden@m" id="rm-mobile-nav-widget-toggle">
	<div class="uk-margin-left" id="rm-move-mobile">
		
	</div>
</div>
<div class="rm-nav-item-search uk-hidden@m" id="rm-mobile-nav-search-toggle">
	<form id="mobile-nav-search-form" class="uk-search uk-search-default uk-width-1-1" action="/search" method="GET" role="search">
        <a href="" id="mobile-search-button" class="uk-search-icon-flip" uk-search-icon></a>
        <input class="uk-search-input uk-border-rounded rm-search-input" type="search" placeholder="{{__('search_text')}}" name="q" style="background-color: #f7f8fa; border: 1px solid #e5e5e529;">
    </form>
</div>

<nav class="rm-mobile-navbar uk-navbar-container uk-hidden@m" uk-navbar>
	<div class="uk-width-1-1" style="margin-left: 0;" uk-grid>
		<div class="uk-width-1-4" style="padding-left: 0;">
			<a class="rm-logo rm-mobile-nav-item uk-logo" href="/" style="font-size: 2rem;">s<b>n</b></a>
		</div>
		<div class="uk-width-1-4">
			<a href="#" id="rm-widget-list" class="rm-mobile-nav-item">
				<span class="uk-margin-small-right" uk-icon="icon: thumbnails; ratio: 1.5"></span>
			</a>
		</div>
		<div class="uk-width-1-4">
			<a href="#" id="rm-type-list" class="rm-mobile-nav-item">
				<span class="uk-margin-small-right" uk-icon="icon: list; ratio: 1.5"></span>
			</a>
		</div>
		<div class="uk-width-1-4">
			<a href="#" id="rm-content-search" class="rm-mobile-nav-item">
				<span class="uk-margin-small-right" uk-icon="icon: search; ratio: 1.5"></span>
			</a>
		</div>
	</div>
</nav>