<div class="uk-width-1-1 rm-content-background uk-border-rounded" style="padding-left: 0;">
	<img src="{{asset('/storage/config/sn-placeholder.jpg')}}" class="rm-content-img rm-border-top-rounded">
	<div class="uk-width-1-1" style="padding: 10px; margin: 0;" uk-grid>
		<div class="uk-width-1-3" style="padding: 0px;">
			<span class="uk-badge rm-badge-color" style="border-radius: 5px !important;">
				{{date('H:i d-m-Y')}}
			</span>
		</div>
		<div class="uk-width-2-3" style="padding: 0px; text-align: right;">
			<div class="addthis_inline_share_toolbox"></div>
		</div>
	</div>
	<div class="uk-width-1-1" style="padding: 10px;">
		<h3>{{__('empty_title')}}</h3>
		<p>{{__('empty_desc')}}</p>
	</div>
</div>