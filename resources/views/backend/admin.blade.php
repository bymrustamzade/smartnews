<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="uk-background-muted">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('/storage/config/logo.ico')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'ftv') }}</title>

    <!-- Styles -->
    <link href="{{ asset('backend/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/quill.snow.css') }}" rel="stylesheet">
</head>
<body>
    @auth
        <div id="app">
            <app-component 
            	appname="{{env('APP_NAME')}}" 
            	locale="{{app()->getLocale()}}"
            	is_superadmin="{{Auth::user()->is_superadmin}}"
				admin_role="{{Auth::user()->permissions->role}}"
				permissions="{{Auth::user()->permissions->allowed}}"
				admin_allowed="{{Auth::user()->admin_allowed}}"
            ></app-component>
        </div>
    @endauth
    <!-- Scripts -->
    <script src="{{ asset('backend/js/app.js') }}"></script>
</body>
</html>