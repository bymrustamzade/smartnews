<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="uk-height-1-1">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'smartnews') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('backend/css/app.css') }}" rel="stylesheet">
</head>
<body class="uk-height-1-1 uk-background-muted">
    <div id="app">
        <main class="py-4">
            @yield('content')
        </main>
        <div class="uk-position-bottom-center uk-position-small">
            <span class="uk-text-small uk-text-muted">© {{date('Y')}} {{ config('app.name', 'smartnews') }} - All rights reserved</span>
        </div>
    </div>
<!-- Scripts -->
<script src="{{ asset('backend/js/app.js') }}" defer></script>
</body>
</html>
