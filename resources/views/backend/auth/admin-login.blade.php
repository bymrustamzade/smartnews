@extends('backend.layouts.admin-app')

@section('content')
<div class="uk-flex uk-flex-center uk-flex-middle uk-background-muted uk-height-viewport">
    <div class="uk-width-medium uk-padding-small">

        <form method="POST" action="{{ route('admin.login.submit') }}">
            @csrf
            <fieldset class="uk-fieldset">
                <legend class="uk-legend uk-text-center uk-text-primary uk-text-bold">{{ config('app.name', 'Laravel') }}</legend>
                <div class="uk-margin">
                    <div class="uk-inline uk-width-1-1">
                        <span class="uk-form-icon uk-form-icon-flip uk-icon" uk-icon="icon: user"></span>
                        <input class="uk-input uk-border-rounded {{ $errors->has('email') ? ' uk-form-danger' : '' }}" required="" placeholder="Email" type="text" name="email" value="{{ old('email') }}" required autofocus>
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-inline uk-width-1-1">
                        <span class="uk-form-icon uk-form-icon-flip uk-icon" uk-icon="icon: lock"></span>
                        <input class="uk-input uk-border-rounded {{ $errors->has('password') ? ' uk-form-danger' : '' }}" required="" placeholder="Password" type="password" name="password" required>
                    </div>
                </div>
                <div class="uk-margin">
                    <label><input class="uk-checkbox uk-border-rounded" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}</label>
                </div>
                <div class="uk-margin">
                    <button type="submit" class="uk-button uk-button-primary uk-button-primary uk-button-large uk-width-1-1 uk-border-rounded">{{ __('Login') }}</button>
                </div>
                
            </fieldset>
        </form>
    </div>
</div>
@if($errors->has('email'))
    <p class="mr-error-email" mr-status="error" style="display: none;">{{ $errors->first('email') }}</p>
@endif
@endsection