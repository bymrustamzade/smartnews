import moment from 'moment'

export default {
  methods: {
    datetimeFormat (date, callbackText = 'Never') {
      try {
        if (!date) {
          throw Error('No date')
        }
        return moment(date).format('D.M.YYYY HH:mm')
      } catch (err) {
        return callbackText
      }
    },
    dateFormat(date, callbackText = 'Never') {
    	try {
			if (!date) {
			  throw Error('No date')
			}
			return moment(date).format('D.M.YYYY')
		} catch (err) {
			return callbackText
		}
    },
    toTimeFormat(date, callbackText = 'Never') {
    	try {
			if (!date) {
			  throw Error('No date')
			}
			return moment(date).format('HH:mm')
		} catch (err) {
			return callbackText
		}
    }
  }
}
