import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const messages = {
	az: require('../../lang/az'),
	ru: require('../../lang/ru'),
	en: require('../../lang/en')
}

export const i18n = new VueI18n({
	locale: 'en',
	fallbackLocale: 'en',
	messages
})