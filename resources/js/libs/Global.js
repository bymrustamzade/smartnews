import languages from '../config/languages'
import privs from '../config/privs'
export default {
	methods: {
		getLanguages () {
			return languages.languages
		},
		hasPermission(route, is_superadmin = 0, admin_allowed = [], permissions = []) {

			var a_allowed = admin_allowed.length > 0 ? JSON.parse(admin_allowed) : [];
			var prms = permissions.length > 0 ? JSON.parse(permissions) : [];

			var only_super_admin = ['permissions', 'permission-edit', 'admins', 'admin-edit', 'admin-add']

			if(only_super_admin.includes(route)){
				return is_superadmin == true || is_superadmin == 1
			}
			var access_free = ['profile', 'profile-edit', 'access-denied', 'page-not-found']
			if(route && !(access_free.includes(route))){
				if(is_superadmin == true || is_superadmin == 1){
					return true
				}else{
					if(Object.keys(a_allowed).length == 0 || (Object.keys(a_allowed).length == 1 && 'helper' in a_allowed)){
						return route in prms
					}else{
						return route in a_allowed
					}
				}
			}else{
				return true
			}
		},
		getPrivs(){
			return privs
		},
		prepareSlug(content){
			var slug = "";
			// Change to lower case
			var titleLower = content.toString().toLowerCase();
			// Letter "e"
			slug = titleLower.replace(/e|é|è|ẽ|ẻ|ẹ|ê|ế|ề|ễ|ể|ệ/gi, 'e');
			// Letter "a"
			slug = slug.replace(/a|á|à|ã|ả|ạ|ă|ắ|ằ|ẵ|ẳ|ặ|â|ấ|ầ|ẫ|ẩ|ậ/gi, 'a');
			// Letter "o"
			slug = slug.replace(/o|ó|ò|õ|ỏ|ọ|ô|ố|ồ|ỗ|ổ|ộ|ơ|ớ|ờ|ỡ|ở|ợ/gi, 'o');
			// Letter "u"
			slug = slug.replace(/u|ú|ù|ũ|ủ|ụ|ư|ứ|ừ|ữ|ử|ự/gi, 'u');
			// Letter "d"
			slug = slug.replace(/đ/gi, 'd');
			// Trim the last whitespace
			slug = slug.replace(/\s*$/g, '');
			// Change whitespace to "-"
			slug = slug.replace(/\s+/g, '-');

			return slug;
		},
		truncate(source, size) {
			return source.length > size ? source.slice(0, size - 1) + "…" : source;
		},
		isDarkMode(){
			return window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
		}
	}
}