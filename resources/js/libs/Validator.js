export default {
  data () {
    return {
      errors: []
    }
  },
  methods: {
    isRequired(item, name){
      if(!item) this.errors.push(name)
      return !item
    }
  }
}
