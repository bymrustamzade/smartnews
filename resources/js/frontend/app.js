if ($(window).width() < 960){
	if($('#rm-move-web').hasClass('done') && !$('#rm-move-mobile').hasClass('done')){
		$('.mover').appendTo('#rm-move-mobile');
		$('#rm-move-web').removeClass('done');
		$('#rm-move-mobile').addClass('done');
	}
}

$( document ).ready(function() {
	//load more functionality
	var offset = 10;
	var endData = false;
	let loadUrl = $('#ajaxUrl').attr('value')
	$('#loadmore').on('scroll', function() {
        if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
        	if(!endData){
        		endData = true;
        		$('#fill-loadmore').append('<div class="loading-content"><div class="loading"><span></span></div></div>');
    			var ajaxRequest = new Promise((resolve, reject) => {
		            $.ajax({
		            	url: loadUrl + "offset=" + offset,
		            	success: function(data){
		            		endData = data == [];
		            		$('#fill-loadmore').append(data);
		            		addthis.layers.refresh();
		            		offset += 10;
		            		$('.loading-content').remove();
		            	}
		            });
		        });
        	}
        }
    })

	//search button click on web
    $('#search-button').click(function(e) {
		e.preventDefault();
		$( "#nav-search-form" ).submit();
	});

	$('#mobile-search-button').click(function(e) {
		e.preventDefault();
		$( "#mobile-nav-search-form" ).submit();
	});

    //list click on mobile
    $("#rm-widget-list").click(function(){
    	$('#rm-mobile-nav-list-toggle').removeClass('rm-open-item');
    	$('#rm-mobile-nav-search-toggle').removeClass('rm-open-item');
		$("#rm-mobile-nav-widget-toggle").toggleClass('rm-open-item');
	});
	$("#rm-type-list").click(function(){
		$('#rm-mobile-nav-search-toggle').removeClass('rm-open-item');
		$("#rm-mobile-nav-widget-toggle").removeClass('rm-open-item');
		$("#rm-mobile-nav-list-toggle").toggleClass('rm-open-item');
	});
	$("#rm-content-search").click(function(){
		$("#rm-mobile-nav-widget-toggle").removeClass('rm-open-item');
		$("#rm-mobile-nav-list-toggle").removeClass('rm-open-item');
		$("#rm-mobile-nav-search-toggle").toggleClass('rm-open-item');
	});

	$(window).on('resize', function(){
		if ($(window).width() > 960){
			if($('#rm-move-mobile').hasClass('done') && !$('#rm-move-web').hasClass('done')){
				$('.mover').appendTo('#rm-move-web')
				$('#rm-move-mobile').removeClass('done');
				$('#rm-move-web').addClass('done');
			}
		}else{
			if($('#rm-move-web').hasClass('done') && !$('#rm-move-mobile').hasClass('done')){
				$('.mover').appendTo('#rm-move-mobile')
				$('#rm-move-web').removeClass('done');
				$('#rm-move-mobile').addClass('done');
			}
		}
	})

	$('#loadmore').scroll(function () { 
		if( $(this).scrollTop() > 600 ) {
			$('.rm-to-top:hidden').show();
		}else{
			$('.rm-to-top:visible').hide();
		}
	})

	$('.rm-to-top').click(function() {
		$('#loadmore').animate({ scrollTop: 0 }, 2000);
	});

	window.addEventListener('resize', appHeight);
	appHeight();
});


function appHeight() {
	const doc = document.documentElement
	doc.style.setProperty('--vh', (window.innerHeight*.01) + 'px');
}
