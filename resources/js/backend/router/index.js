import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '../components/pages/Dashboard'
import Images from '../components/pages/Images'
import Broadcasts from '../components/pages/Broadcasts'
import Gallery from '../components/pages/Gallery'
import Profile from '../components/pages/Profile'
import Localization from '../components/pages/Localization'
import LocalizationEdit from '../components/pages/subpages/Localization/Edit'
import Permissions from '../components/pages/Permissions'
import PermissionEdit from '../components/pages/subpages/Permission/Edit'
import ProfileEdit from '../components/pages/subpages/Profile/Edit'
import Admins from '../components/pages/Admins'
import AdminAdd from '../components/pages/subpages/Admins/Add'
import AdminEdit from '../components/pages/subpages/Admins/Edit'
import Content from '../components/pages/Content'
import ContentAdd from '../components/pages/subpages/Content/Add'
import ContentEdit from '../components/pages/subpages/Content/Edit'
import TvSchedules from '../components/pages/TvSchedules'
import TvSchedulesEdit from '../components/pages/subpages/TvSchedules/Edit'
import Settings from '../components/pages/Settings'
import SettingsEdit from '../components/pages/subpages/Settings/Edit'

import Emails from '../components/pages/Emails'
import EmailSend from '../components/pages/subpages/Emails/Send'

import AccessDenied from '../components/pages/AccessDenied'
import PageNotFound from '../components/pages/PageNotFound'


import Types from '../components/pages/Types'
import TypeAdd from '../components/pages/subpages/Types/Add'
import TypeEdit from '../components/pages/subpages/Types/Edit'

Vue.use(Router)

export default new Router({
  routes: [
  	{ path: '/', name: 'dashboard', component: Dashboard },
  	{ path: '/profile', name: 'profile', component: Profile },
  	{ path: '/images', name: 'images', component: Images },
  	{ path: '/broadcasts', name: 'broadcasts', component: Broadcasts },
  	{ path: '/gallery', name: 'gallery', component: Gallery },
  	{ path: '/localization', name: 'localization', component: Localization },
  	{ path: '/localization-edit/:position/:lang', name: 'localization-edit', component: LocalizationEdit },
  	{ path: '/permissions', name: 'permissions', component: Permissions },
  	{ path: '/permission-edit/:id', name: 'permission-edit', component: PermissionEdit },
  	{ path: '/profile-edit', name: 'profile-edit', component: ProfileEdit },
  	{ path: '/admins', name: 'admins', component: Admins },
  	{ path: '/admin-add', name: 'admin-add', component: AdminAdd },
  	{ path: '/admin-edit/:id', name: 'admin-edit', component: AdminEdit },
  	{ path: '/types', name: 'types', component: Types },
  	{ path: '/type-add', name: 'type-add', component: TypeAdd },
  	{ path: '/type-edit/:id', name: 'type-edit', component: TypeEdit },
  	{ path: '/content', name: 'content', component: Content },
  	{ path: '/content-add', name: 'content-add', component: ContentAdd },
  	{ path: '/content-edit/:id', name: 'content-edit', component: ContentEdit },
  	// { path: '/tv-schedules', name: 'tv-schedules', component: TvSchedules },
  	// { path: '/tv-schedules-edit/:day/:lang', name: 'tv-schedules-edit', component: TvSchedulesEdit },
  	{ path: '/settings', name: 'settings', component: Settings },
  	{ path: '/settings-edit/:id', name: 'settings-edit', component: SettingsEdit },

  	{ path: '/emails', name: 'emails', component: Emails },
  	{ path: '/email-send', name: 'email-send', component: EmailSend },

  	{ path: '/access-denied', name: 'access-denied', component: AccessDenied },
    { path: "*", component: PageNotFound }
  ]
})