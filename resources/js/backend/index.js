
//UIKIT things
window.UIkit = require('uikit')
import Icons from 'uikit/dist/js/uikit-icons'
UIkit.use(Icons);

//Quill
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

//Vue Things
window.Vue = require('vue')
import router from './router'
import VueResource from 'vue-resource'
import VueSlugify  from 'vue-slugify'
import VueI18n from 'vue-i18n'
Vue.use(VueResource)
Vue.use(VueSlugify)
Vue.use(VueI18n)


//Vue Components
import AppComponent from './components/App'
import AdminComponent from './components/AdminComponent'
Vue.component('app-component', AppComponent)
Vue.component('admin-component', AdminComponent)

//Layouts
import HeaderLayout from './components/layouts/HeaderLayout'
import ActionPanelLayout from './components/layouts/ActionPanelLayout'
import TableLayout from './components/layouts/TableLayout'
import ActionBulkLayout from './components/layouts/ActionBulkLayout'
import DragDropSelectLayout from './components/layouts/DragDropSelectLayout'
import ImagesLayout from './components/layouts/ImagesLayout'
import TextEditorLayout from './components/layouts/TextEditorLayout'
Vue.component('header-layout', HeaderLayout)
Vue.component('action-panel-layout', ActionPanelLayout)
Vue.component('table-layout', TableLayout)
Vue.component('action-bulk-layout', ActionBulkLayout)
Vue.component('dragdrop-select-layout', DragDropSelectLayout)
Vue.component('images-layout', ImagesLayout)
Vue.component('texteditor-layout', TextEditorLayout)

//Vue mixins
import Api from '../libs/Api'
import Date from '../libs/Date'
import { i18n } from '../libs/i18n'
import Global from '../libs/Global'
Vue.mixin(Api)
Vue.mixin(Date)
Vue.mixin(Global)

const app = new Vue({
	i18n,
    el: '#app',
    router
});

$(document).ready(function () {
	if($('.mr-error-email').attr('mr-status') == 'error'){
		UIkit.notification({
	        message: $('.mr-error-email').text(), 
	        status: 'danger'
	    })
    }
});