<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    "accepted" => "':attribute' qəbul edilməlidir.",
    "active_url" => "':attribute' etibarlı URL deyil.",
    "after" => "':date' tarixdən sonra olmalıdır.",
    "after_or_equal" => "':attribute' ya :date tarixdən sonra və ya eyni bu tarix olmalıdır.",
    "alpha" => "':attribute' yalnız hərflərdən ibarət olmmalıdır.",
    "alpha_dash" => "Bu yalnız hərflərdən, rəqəmlərdən, tire və ya altdan xəttdən ibarət olmalıdır",
    "alpha_num" => "Bu yalnız hərflərdən və rəqəmlərdən ibarət olmalıdır.",
    "array" => "The must be an array.",
    "before" => "Bu :date tarixindən evvəl olmalıdır.",
    "before_or_equal" => "Bu :date tarixindən evvəl və ya eyni bu tarix olmalıdır.",
    "between" => [
        "numeric" => "Bu :min və :max aralığında olmalıdır.",
        "file" => "Bu :min və :max kilobayt aralığında olmalıdır.",
        "string" => "Bu :min və :max hərfləri aralığında olmalıdır.",
        "array" => "Bu :min və :max maddələr aralığında olmalıdır.",
    ],
    "boolean" => "':attribute' yalnış və ya doğru olmalıdır.",
    "confirmed" => "':attribute' təsdiqləməsi uyğun gəlmir.",
    "date" => "':attribute' tarixi yalnışdır.",
    "date_equals" => "Bu :date tarixiylə eyni olmalıdır.",
    "date_format" => "Bu tarix formatı :format formatına uyğun deyil.",
    "different" => "':attribute' və ':other' fərqli olmalıdır.",
    "digits" => "Bu :digits tipli rəqəm olmalıdır.",
    "digits_between" => "Bu :min və :max rəqəmləri aralığında olmalıdır.",
    "dimensions" => "Bu şəkil ölçüləri uyğun gəlmir.",
    "distinct" => "Bu sahə dublikatdır.",
    "email" => "Bu emayl düzgün formatda deyil.",
    "ends_with" => "Bunlardan biriylə bitməlidir: :values",
    "exists" => "Bu seçilən yalnışdır.",
    "file" => "Bu fayl olmalıdır.",
    "filled" => "Bu sahə boş olmamalıdır.",
    "gt" => [
        "numeric" => "Bu :value daha böyük olmalıdır.",
        "file" => "Bu :value kilobaytdan daha böyük olmalıdır.",
        "string" => "Bu :value hərfdən daha böyük olmalıdır.",
        "array" => "Bu :value daha çox olmalıdır.",
    ],
    "gte" => [
        "numeric" => "Bu :value daha böyük və ya bərabər olmalıdır.",
        "file" => "Bu :value kilobaytdan daha böyük və ya bərabər olmalıdır.",
        "string" => "Bu :value hərfdən daha böyük və ya eyni olmalıdır.",
        "array" => "Bu :value daha çox və ya eyni olmalıdır.",
    ],
    "image" => "Bu şəkil formatında olmalıdır.",
    "in" => "Bu seçilən yalnışdır.",
    "in_array" => "Bu sahə :other yoxdur.",
    "integer" => "Bu rəqəm olmalıdır.",
    "ip" => "Bu düzgün ip ünvan olmalıdır.",
    "ipv4" => "Bu düzgün IPv4 ünvan olmalıdır.",
    "ipv6" => "Bu düzgün IPv6 ünvan olmalıdır.",
    "json" => "Bu düzgün JSON formatında olmalıdır.",
    "lt" => [
        "numeric" => "Bu :value az olmalıdır.",
        "file" => "Bu :value kilobaytdan az olmalıdır.",
        "string" => "Bu :value hərfdən aşağı olmalıdır.",
        "array" => "Bu :value az olmalıdır.",
    ],
    "lte" => [
        "numeric" => "Bu :value az və ya eyni olmalıdır.",
        "file" => "Bu :value kilobaytdan az və ya eyni olmalıdır.",
        "string" => "Bu :value hərfdən aşağı və ya eyni olmalıdır.",
        "array" => "Bu :value az və ya eyni olmalıdır.",
    ],
    "max" => [
        "numeric" => "Bu maximum :max ibarət olmalıdır.",
        "file" => "Bu maximum :max kilobaytdan ibarət olmalıdır.",
        "string" => "Bu maximum :max hərfdən ibarət olmalıdır.",
        "array" => "Bu maximum :max ibarət olmalıdır.",
    ],
    "mimes" => "Bu fayl, :values fayl tipindən olmalıdır.",
    "mimetypes" => "Bu  fayl, :values fayl tipindən olmalıdır.",
    "min" => [
        "numeric" => "Bu minimum :min ibarət olmalıdır.",
        "file" => "Bu minimum :min kilobaytdan ibarət olmalıdır.",
        "string" => "Bu minimum :min hərfdən ibarət olmalıdır.",
        "array" => "Bu minimum :min ibarət olmalıdır",
    ],
    "not_in" => "Bu seçilən yalnışdır.",
    "not_regex" => "Bu formatı yalnışdır.",
    "numeric" => "Bu rəqəm olmalıdır.",
    "present" => "Bu sahəsi mövcud olmalıdır.",
    "regex" => "Bu formatı yalnışdır.",
    "required" => "Bu sahənin doldurulması vacibdir.",
    "required_if" => "The field is required when :other is :value.",
    "required_unless" => "The field is required unless :other is in :values.",
    "required_with" => "The field is required when :values is present.",
    "required_with_all" => "The field is required when :values are present.",
    "required_without" => "The field is required when :values is not present.",
    "required_without_all" => "The field is required when none of :values are present.",
    "same" => "Bu və :other uyğun olmalıdır.",
    "size" => [
        "numeric" => "The must be :size.",
        "file" => "The must be :size kilobytes.",
        "string" => "The must be :size characters.",
        "array" => "The must contain :size items.",
    ],
    "starts_with" => "Bu bunlardan :values biriylə başlamalıdır",
    "string" => "Bu string(yazı) formatında olmalıdır.",
    "timezone" => "The must be a valid zone.",
    "unique" => "Bu artıq mövcuddur.",
    "uploaded" => "Bu yükləməkdə xəta baş verdi.",
    "url" => "Bu format yalnışdır.",
    "uuid" => "The must be a valid UUID.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages fors using the
    | convention .rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given rule.
    |
    */

    "custom" => [
        "attribute-name" => [
            "rule-name" => "custom-message",
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validations
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    "attributes" => [
    	"password" => "şifrə",
    	"password_new" => "yeni şifrə",
    	"password_new_confirmation" => "yeni şifrə təkrarı"
    ],

];
