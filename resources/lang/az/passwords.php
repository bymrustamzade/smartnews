<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Sizin şifrəniz bərpa edildi!',
    'sent' => 'Şifrənizin bərpa edilməsi üçün sizə emayl göndərildi!',
    'token' => 'Şifrənin bəröasə üçün olan nişanə yalnışdır.',
    'user' => "Belə bir emaylı olan istifadəçi tapılmadı. ",

];
