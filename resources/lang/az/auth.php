<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Daxil etdiyiniz məlumatlar yalnışdır',
    'throttle' => 'Həddən çox yalnış məlumat daxil edildi. Zəhmət olmasa :seconds saniyə sonra təkrar yoxlayın.',

];
