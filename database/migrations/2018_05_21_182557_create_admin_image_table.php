<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_images', function (Blueprint $table) {
            $table->unsignedInteger('id_admin');
            $table->unsignedInteger('id_image');
            $table->boolean('is_main')->unsigned()->default(0);
            $table->timestampsTz();
            $table->foreign('id_admin', 'id_admin_ref')->references('id_admin')->on('admins');
            $table->foreign('id_image', 'id_admin_image_ref')->references('id_image')->on('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_images');
    }
}
