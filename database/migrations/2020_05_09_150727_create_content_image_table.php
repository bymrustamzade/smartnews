<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_images', function (Blueprint $table) {
            $table->unsignedBigInteger('id_content');
            $table->unsignedInteger('id_image');
            $table->boolean('is_main')->unsigned()->default(0);
            $table->timestampsTz();
            $table->foreign('id_content', 'id_content_image_ref')->references('id_content')->on('content');
            $table->foreign('id_image', 'id_image_content_ref')->references('id_image')->on('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_image');
    }
}
