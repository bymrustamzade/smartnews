<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlltypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types', function (Blueprint $table) {
            $table->bigIncrements('id_type');
            $table->string('type', 255);
            $table->string('internal_name', 255)->unique();
            $table->string('name_az', 255);
            $table->string('name_en', 255);
            $table->string('name_ru', 255);
            $table->integer('sort')->nullable();
            $table->boolean('is_active')->default(0);
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types');
    }
}
