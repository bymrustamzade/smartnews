<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image', function (Blueprint $table) {
            $table->increments('id_image');
            $table->string('filename', 250);
            $table->string('path', 250);
            $table->unsignedInteger('width');
            $table->unsignedInteger('height');
            $table->unsignedInteger('filesize');
            $table->boolean('public')->unsigned()->default(1);
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image');
    }
}
