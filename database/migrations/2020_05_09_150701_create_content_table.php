<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->bigIncrements('id_content');
            $table->unsignedBigInteger('id_type');
            $table->string('internal_name', 255);
            $table->string('language', 2);
            $table->string('title', 255);
            $table->string('slug', 255);
            $table->text('topics')->nullable();
            $table->text('description');
            $table->longText('body');
            $table->string('seo_title', 255)->nullable();
            $table->string('seo_keywords', 255)->nullable();
            $table->string('seo_description', 255)->nullable();
            $table->string('seo_robots', 255)->nullable();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_paid')->default(0);
            $table->boolean('is_big')->default(0);
            $table->timestampsTz();
            $table->foreign('id_type', 'id_content_type_ref')
            		->references('id_type')
            		->on('types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}
