<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageThumbnailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_thumbnail', function (Blueprint $table) {
            $table->increments('id_image_thumbnail');
            $table->unsignedInteger('id_image');
            $table->string('filename', 250);
            $table->string('path', 250);
            $table->unsignedInteger('width');
            $table->unsignedInteger('height');
            $table->unsignedInteger('filesize');
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_thumbnail');
    }
}
