<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admins')->delete();
        
        \DB::table('admins')->insert(array (
            0 => 
            array (
                'id_admin' => 1,
                'name' => 'Mahammad',
                'email' => 'user@example.com',
                'password' => '$2y$12$YYpqYjUkDRcO8gHTAUpuFe1.Ca7jaBEeASXh1xBMVxzyuOjYrlH2i',//password = 12341234
                'remember_token' => null,
                'is_superadmin' => 1,
                'role' => 1,
                'created_at' => '2018-06-15 14:31:05'
            ),
        ));
    }
}