<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id_permission' => 1,
                'role' => 'moderator',
                'allowed' => json_encode(['holder' => true]),
                'created_at' => '2018-06-15 14:31:05'
            ),
            1 => 
            array (
                'id_permission' => 2,
                'role' => 'translator',
                'allowed' => json_encode(['holder' => true]),
                'created_at' => '2018-06-15 14:31:05'
            ),
        ));
    }
}
