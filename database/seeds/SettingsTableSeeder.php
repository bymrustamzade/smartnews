<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id_settings' => 1,
                'language' => 'az',
                'content' => json_encode([]),
                'created_at' => '2019-11-02 15:18:04'
            ),
            1 => 
            array (
                'id_settings' => 2,
                'language' => 'en',
                'content' => json_encode([]),
                'created_at' => '2019-11-02 15:18:05'
            ),
            2 => 
            array (
                'id_settings' => 3,
                'language' => 'ru',
                'content' => json_encode([]),
                'created_at' => '2019-11-02 15:18:06'
            ),
        ));
    }
}
