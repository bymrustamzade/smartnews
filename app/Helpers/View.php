<?php

namespace App\Helpers;

class View {

	public static function getYoutubeEmbedUrl($videolink)
	{
		preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $videolink, $match);
		return $match[1];
	}
}