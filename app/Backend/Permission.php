<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $primaryKey = 'id_permission';
    protected $table = 'permissions';

	public function admins()
	{
		return $this->hasMany(Admin::class, 'role');
	}
}
