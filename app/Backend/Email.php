<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
	protected $primaryKey = 'id_email';
    protected $table = 'emails';
}
