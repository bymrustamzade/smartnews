<?php

namespace App\Backend;

use App\Libs\Image;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';
    protected $primaryKey = 'id_admin';
    protected $table = 'admins';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function images(){
        return $this->belongsToMany(Image::class, 'admin_images', 'id_admin', 'id_image')->select('image.*','admin_images.is_main');
    }

    public function permissions(){
    	return $this->belongsTo(Permission::class, 'role');
    }
}
