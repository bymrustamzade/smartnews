<?php

namespace App;

use App\Libs\Image;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
	protected $primaryKey = 'id_content';
    protected $table = 'content';


    public function images(){
        return $this->belongsToMany(Image::class, 'content_images', 'id_content', 'id_image')->select('image.*','content_images.is_main');
    }

    public function types(){
    	return $this->belongsTo(Types::class, 'id_type');
    }
}
