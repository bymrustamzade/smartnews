<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Broadcast extends Model
{
    use Notifiable;

    protected $table = "broadcast";
    protected $primaryKey = 'id_broadcast';
}
