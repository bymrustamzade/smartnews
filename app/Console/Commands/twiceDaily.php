<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class twiceDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'widget:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Widget update is for getting daily weather and coronovirus statistics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = "https://apps.elfsight.com/p/boot/?w=eb4d6c3e-5228-437c-bea8-7cd7b018a594%2C53d8f7d7-e5b7-45b2-bb78-384bc27766d3";

    	$data = file_get_contents($url);

    	$fill = DB::table('widget')->insert([
                    'data' => $data,
                    'created_at' => date('Y-m-d H:i:s')
                ]);

    }
}
