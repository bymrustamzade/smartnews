<?php

namespace App\Mail;

use Illuminate\Queue\SerializesModels;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;

class Email extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	if(empty($this->data['attachments'])){
        	return $this->subject($this->data['title'])->view('backend.emails.email');
    	}else{
    		$email = $this->subject($this->data['title'])->view('backend.emails.email');

    		foreach ($this->data['attachments'] as $attachment) {
    			$email->attach(
					$attachment->getRealPath(),
	                [
	                    'as' => $attachment->getClientOriginalName(),
	                    'mime' => $attachment->getClientMimeType(),
	                ]
	            );
    		}
    		return $email;
    	}
    }
}
