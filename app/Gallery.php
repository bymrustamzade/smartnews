<?php

namespace App;

use App\Libs\Image;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $primaryKey = 'id_gallery';
    protected $table = 'gallery';

    public function image(){
        return $this->belongsTo(Image::class, 'id_image');
    }
}
