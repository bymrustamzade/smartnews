<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function hasAccess($admin, $page){
    	if($admin->is_superadmin){
    		return true;
    	}else{
    		$admin_allowed = json_decode($admin->admin_allowed, true);
    		$priv_allowed = json_decode($admin->permissions->allowed, true);

    		if(empty($admin_allowed) || (count($admin_allowed) === 1 && array_key_exists('holder', $admin_allowed))){
    			return array_key_exists($page, $priv_allowed);
    		}else{
    			return array_key_exists($page, $admin_allowed);
    		}
    	}
    }

    protected function setMainImage($table, $field, $field_value, $id_image){

    	DB::table($table)->where($field, $field_value)->update(['is_main' => 0]);
    	DB::table($table)->where(['id_image' => $id_image, $field => $field_value])->update(['is_main' => 1]);
    	$result['success'] = "success.update";
        return response()->json($result);
    }

    public function getImages($table, $field, $field_value, Request $request, $main_raw = ''){
        $r_all = $request->all();
    	
        if(!array_key_exists('search', $request->all())){
            abort(404);
        }

        $availableSearch = ['filename', 'path', 'width', 'height', 'created_at'];
        if(!in_array($request->searchBy, $availableSearch)){
            return response()->json([]);
        }

    	$images = DB::table($table);
		if($field && $field_value){
			$images->where([[$table . '.' . $field, $field_value],['image.' . $request->searchBy, 'like', '%'.$request->search.'%']]);
		}else{
			$images->where('image.' . $request->searchBy, 'like', '%'.$request->search.'%');
		}
	    $images->selectRaw('image.*,'.$main_raw.'
	                image_thumbnail.filename as t_filename, 
	                image_thumbnail.path as t_path,
	                image_thumbnail.width as t_width,
	                image_thumbnail.height as t_height'
	                )
		   	->join('image', function ($join) use ($table) {
		            $join->on($table . '.id_image', '=', 'image.id_image');
		    })
		    ->leftJoin('image_thumbnail', 'image_thumbnail.id_image', '=', 'image.id_image')
		    ->orderBy('image.' . $request->orderBy, $request->orderDir);

		$data = $images->paginate((int)$request->limit);

        $result = [
            "items" => $data->items(),
            "pager" => [
                "currentPage" => $data->currentPage(),
                "currentLimit" => $data->perPage(),
                "totalPages" => $data->lastPage(),
                "totalItems" => $data->total(),
            ]
        ];

        return response()->json($result);
    }
}
