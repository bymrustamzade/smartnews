<?php

namespace App\Http\Controllers\Frontend;

use DB;
use App\Broadcast;
use Illuminate\Http\Request;

class BroadcastController extends Controller
{
    public function index()
	{
		$videos = Broadcast::where('public', '=', 1)
								->orderBy('created_at')
								->paginate(25);

		$data = [];

		foreach ($videos as $video) {
			$arr = [];
			$arr['id_broadcast'] = $video->id_broadcast;
			$arr['name'] = $video->name;
			$arr['internal_name'] = $video->internal_name;
			$arr['path'] = $video->path;
			$arr['created_at'] = $video->created_at->format('H:i d.m.y');
			$data[] = $arr;
		}

		$result = [
			"items" => $data,
			"pager" => [
				"currentPage" => $videos->currentPage(),
				"currentLimit" => $videos->perPage(),
				"totalPages" => $videos->lastPage(),
				"totalItems" => $videos->total(),
			]
		];

		return view('frontend/broadcast', ['data' => $result]);
	}
}
