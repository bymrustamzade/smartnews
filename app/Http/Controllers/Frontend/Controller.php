<?php

namespace App\Http\Controllers\Frontend;

use View;
use Session;
use App\Types;
use App\Content;
use App\Settings;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        if (!Session::has('locale')) {
            $lang = 'az';
        } else {
            $lang = Session::get('locale');
        }

        $settings = Settings::where('language', '=', $lang)->first();

        View::share('categories', $this->getTypes());
        View::share('content_list', $this->getContentList());
        View::share('settings', json_decode($settings->content));
        View::share('languages', ['az', 'ru']);
    }

    public function getTypes()
    {
        if (!Session::has('locale')) {
            $lang = 'az';
        } else {
            $lang = Session::get('locale');
        }

        $types = Types::where('is_active', '=', 1)
            ->orderBy('sort', 'asc')
            ->get();

        $typedata = [];

        foreach ($types as $type) {
            $az = $type->name_az;
            $en = $type->name_en;
            $ru = $type->name_ru;
            $arr['key'] = $type->id_type;
            $arr['text'] = ${$lang};
            $typedata[] = $arr;
        }

        return $typedata;
    }

    function truncate($text, $chars = 25)
    {
        if (strlen($text) <= $chars) {
            return $text;
        }
        $text = $text . " ";
        $text = substr($text, 0, $chars);
        $text = substr($text, 0, strrpos($text, ' '));
        $text = $text . "...";
        return $text;
    }

    private function getContentList()
    {
        if (!Session::has('locale')) {
            $lang = 'az';
        } else {
            $lang = Session::get('locale');
        }

        $content = Content::where('is_active', '=', 1)
            ->where('language', '=', $lang)
            ->orderBy('created_at', 'desc')
            ->with('images')
            ->limit(25)
            ->get();

        $data = [];

        foreach ($content as $c) {
            $arr = [];
            foreach ($c->images as $image) {
                if ($image->is_main == 1) {
                    $arr['img'] = $image->path . $image->filename;
                    $arr['img_path'] = $image->path;
                    $arr['img_filename'] = $image->filename;
                }
            }

            $arr['title'] = $c->title;
            $arr['slug'] = $c->slug;
            $arr['description'] = $this->truncate($c->description, 150);
            $arr['created_at'] = $c->created_at->getTimestamp();
            $arr['url'] = '/v/' . $c->slug;
            $arr['is_paid'] = true;
            $data[] = $arr;
        }

        return $data;
    }


    public function getPaid()
    {
        if (!Session::has('locale')) {
            $lang = 'az';
        } else {
            $lang = Session::get('locale');
        }

        $content = Content::where('is_active', '=', 1)
            ->where('language', '=', $lang)
            ->where('is_paid', '=', 1)
            ->orderBy('created_at', 'desc')
            ->limit(3)
            ->get();
        $data = [];

        foreach ($content as $c) {
            $arr = [];
            foreach ($c->images as $image) {
                if ($image->is_main == 1) {
                    $arr['img'] = $image->path . $image->filename;
                    $arr['img_path'] = $image->path;
                    $arr['img_filename'] = $image->filename;
                }
            }

            $arr['title'] = $c->title;
            $arr['slug'] = $c->slug;
            $arr['description'] = $c->description;
            $arr['created_at'] = $c->created_at->getTimestamp();
            $arr['url'] = '/v/' . $c->slug;
            $arr['is_paid'] = true;
            $arr['is_big'] = $c->is_big;
            $data[] = $arr;
        }

        return $data;
    }
}
