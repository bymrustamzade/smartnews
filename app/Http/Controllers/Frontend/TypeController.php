<?php

namespace App\Http\Controllers\Frontend;

use App\Types;
use App\Content;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TypeController extends Controller
{

    public function index($id_type)
    {
        $data = [];

        $types = Types::where('is_active', '=', 1)
            ->orderBy('sort')
            ->get();

        $typedata = [];

        foreach ($types as $type) {
            $az = $type->name_az;
            $en = $type->name_en;
            $ru = $type->name_ru;
            $arr['key'] = $type->id_type;
            $arr['text'] = ${app()->getLocale()};
            $typedata[] = $arr;
        }

        $data['content'] = $this->fetchData($id_type, 0);

        return view('frontend/type', ['data' => $data, 'categories' => $typedata]);
    }

    public function loadMore($id_type, Request $request)
    {
        if ($request->ajax()) {
            $data['content'] = $this->fetchData($id_type, $request->offset);
        } else {
            abort(404);
        }

        return view('layouts.content', compact('data'))->render();
    }

    private function fetchData($id_type, $offset)
    {
        $content = $this->getData($id_type, $offset, 10);

        if (!empty($content)) {
            $pn = $this->getPaid();
            $content = array_merge($content, $pn);
            shuffle($content);
        }

        usort($content, function ($a, $b) {
            return $a['is_paid'] || $a['created_at'] < $b['created_at'];
        });

        return $content;
    }


    public function getData($id_type, $offset = 0, $limit = 10)
    {
        $atype = Types::where('is_active', '=', 1)
            ->where('id_type', '=', $id_type)
            ->first();

        if (empty($atype)) {
            abort(404);
        }

        $content = Content::where('is_active', '=', 1)
            ->where('id_type', '=', $id_type)
            ->where('is_paid', '!=', 1)
            ->where('language', '=', app()->getLocale())
            ->with('images')
            ->offset($offset)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get();

        $data = [];

        foreach ($content as $c) {
            $arr = [];
            foreach ($c->images as $image) {
                if ($image->is_main == 1) {
                    $arr['img'] = $image->path . $image->filename;
                    $arr['img_path'] = $image->path;
                    $arr['img_filename'] = $image->filename;
                }
            }

            $arr['title'] = $c->title;
            $arr['slug'] = $c->slug;
            $arr['description'] = $c->description;
            $arr['created_at'] = $c->created_at->getTimestamp();
            $arr['url'] = '/v/' . $c->slug;
            $arr['is_paid'] = false;
            $arr['is_big'] = $c->is_big;
            $data[] = $arr;
        }

        return $data;
    }
}
