<?php

namespace App\Http\Controllers\Frontend;

use App\Content;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TopicController extends Controller
{

    public function index($topic)
    {
        $data = [];
        $data['content'] = $this->fetchData($topic, 0);

        return view('frontend/topic', ['data' => $data, 'categories' => $this->getTypes()]);
    }

    public function loadMore($topic, Request $request)
    {
        if ($request->ajax()) {
            $data['content'] = $this->fetchData($topic, $request->offset);
        } else {
            abort(404);
        }

        return view('layouts.content', compact('data'))->render();
    }

    private function fetchData($topic, $offset)
    {
        $content = $this->getData($topic, $offset, 10);

        if (!empty($content) && count($content) > 2) {
            $pn = $this->getPaid();
            $content = array_merge($content, $pn);
            shuffle($content);
        }

        usort($content, function ($a, $b) {
            return $a['is_paid'] || $a['created_at'] < $b['created_at'];
        });

        return $content;
    }


    public function getData($topic, $offset = 0, $limit = 10)
    {
        $content = Content::where('is_active', '=', 1)
            ->where('topics', 'like', '%' . $topic . '%')
            ->where('language', '=', app()->getLocale())
            ->with('images')
            ->offset($offset)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get();

        if (empty($content)) {
            abort(404);
        }

        $data = [];

        foreach ($content as $c) {
            $arr = [];
            foreach ($c->images as $image) {
                if ($image->is_main == 1) {
                    $arr['img'] = $image->path . $image->filename;
                    $arr['img_path'] = $image->path;
                    $arr['img_filename'] = $image->filename;
                }
            }

            $arr['title'] = $c->title;
            $arr['slug'] = $c->slug;
            $arr['description'] = $c->description;
            $arr['created_at'] = $c->created_at->getTimestamp();
            $arr['url'] = '/v/' . $c->slug;
            $arr['is_paid'] = false;
            $arr['is_big'] = $c->is_big;
            $data[] = $arr;
        }

        return $data;
    }
}
