<?php

namespace App\Http\Controllers\Frontend;

use DB;
use App\Types;
use App\Content;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function sitemap() 
    {
		$types = Types::where('is_active', '=', 1)->orderBy('sort')->get();

    	$contentdata = Content::where('is_active', '=', 1)
						->orderBy('created_at', 'desc')
						->get();

	    return view('frontend/sitemap', [
	    	'types' => $types,
	    	'contentdata' => $contentdata,
	    ]);
	}

	public function getWidgetData()
	{
		$widget = DB::table('widget')
	                ->orderBy('id', 'desc')
	                ->first();
	    return response(json_decode($widget->data, true));
	}
}
