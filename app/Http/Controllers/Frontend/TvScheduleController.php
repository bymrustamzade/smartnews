<?php

namespace App\Http\Controllers\Frontend;

use DB;
use App\News;
use Carbon\Carbon;
use App\TvSchedule;
use Illuminate\Http\Request;

class TvScheduleController extends Controller
{
	private function prepareDates()
    {
    	$startDate = Carbon::today();
    	$endDate = Carbon::today()->add(1, 'month');
		$data = [];

		while ($startDate->lte($endDate)){
			$data[] = $startDate->format('d-m-Y');
			$startDate->addDay();
		}
		return $data;
    }

	public function index()
	{
		$days = $this->prepareDates();
    	$startDate = Carbon::today();
    	$data = [];

    	$tvschedules = TvSchedule::where('day', '>=', $startDate)
    								->where('language', app()->getLocale())
									->orderBy('day')
									->limit(14)
    								->get();

    	foreach ($tvschedules as $item) {
    		$arr = [];
    		$day = Carbon::parse($item->day);
    		$arr['day'] = $day->format('d.m.y');
    		$arr['data'] = json_decode($item->data);
    		$data[] = $arr;
    	}

        return view('frontend/tvschedule', ['data' => $data]);
	}
    
}
