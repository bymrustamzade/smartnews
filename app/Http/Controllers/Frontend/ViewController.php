<?php

namespace App\Http\Controllers\Frontend;

use App\Content;
use Carbon\Carbon;
use App\TvSchedule;
use Illuminate\Http\Request;

class ViewController extends Controller
{
    public function index(Request $request)
    {
        $content = Content::where('is_active', '=', 1)
            ->where('slug', '=', $request->slug)
            ->with('images')
            ->first();

        if (empty($content)) {
            abort(404);
        }

        $data = [];

        $data['title'] = $content->title;
        $data['id_alltype'] = $content->id_alltype;
        $data['slug'] = $content->slug;
        $data['description'] = $content->description;
        $data['body'] = $content->body;
        $data['created_at'] = $content->created_at->format('H:i d.m.y');
        $data['seo_title'] = $content->seo_title != '' ? $content->seo_title : $content->title;
        $data['seo_keywords'] = $content->seo_keywords;
        $data['seo_description'] = $content->seo_description != '' ? $content->seo_description : $content->description;
        $data['seo_robots'] = $content->seo_robots;
        $data['main_img'] = '';
        $data['img_path'] = '';
        $data['img_filename'] = '';
        $data['images'] = [];

        if ($content->topics != '') {
            $topics = explode(',', $content->topics);

            foreach ($topics as $topic) {
                $data['topics'][] = rtrim(ltrim($topic));
            }
        } else {
            $data['topics'] = [];
        }

        foreach ($content->images as $image) {
            if ($image->is_main == 1) {
                $data['main_img'] = $image->path . $image->filename;
                $data['img_path'] = $image->path;
                $data['img_filename'] = $image->filename;
            } else {
                $data['images'][] = $image->path . $image->filename;
            }
        }

        if (empty($data['main_img'])) {
            $data['main_img'] = asset('/storage/config/sn-placeholder.jpg');
        }

        return view('frontend/view', ['data' => $data, 'categories' => $this->getTypes()]);

    }
}
