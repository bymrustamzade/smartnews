<?php

namespace App\Http\Controllers\Frontend;

use App\Content;
use Carbon\Carbon;
use App\TvSchedule;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        $data = [];
        $data['content'] = $this->fetchData(0);

        return view('frontend/home', ['data' => $data]);
    }

    public function loadMore(Request $request)
    {
        if ($request->ajax()) {
            $data['content'] = $this->fetchData($request->offset);
        } else {
            abort(404);
        }

        return view('layouts.content', compact('data'))->render();
    }

    public function fetchData($offset)
    {
        $content = $this->getData($offset, 10);

        if (!empty($content)) {
            $pn = $this->getPaid();
            $content = array_merge($content, $pn);
            shuffle($content);
        }

        usort($content, function ($a, $b) {
            return $a['is_paid'] || $a['created_at'] < $b['created_at'];
        });

        return $content;
    }


    private function getData($offset = 0, $limit = 10)
    {
        $content = Content::where('is_active', '=', 1)
            ->where('is_paid', '!=', 1)
            ->where('language', '=', app()->getLocale())
            ->with('images')
            ->offset($offset)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get();

        $data = [];

        foreach ($content as $c) {
            $arr = [];
            foreach ($c->images as $image) {
                if ($image->is_main == 1) {
                    $arr['img'] = $image->path . $image->filename;
                    $arr['img_path'] = $image->path;
                    $arr['img_filename'] = $image->filename;
                }
            }

            $arr['title'] = $c->title;
            $arr['slug'] = $c->slug;
            $arr['description'] = $c->description;
            $arr['created_at'] = $c->created_at->getTimestamp();
            $arr['url'] = '/v/' . $c->slug;
            $arr['is_paid'] = false;
            $arr['is_big'] = $c->is_big;
            $data[] = $arr;
        }

        return $data;
    }
}
