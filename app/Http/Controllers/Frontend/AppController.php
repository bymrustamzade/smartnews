<?php

namespace App\Http\Controllers\Frontend;

use App\News;
use App\Settings;
use App\AllTypes;
use Illuminate\Http\Request;

class AppController extends Controller
{

    public function index()
    {
    	$newstypes = AllTypes::where('is_active', '=', 1)->where('type', '=', 'news')->orderBy('sort')->get();

    	$settings = Settings::where('language', '=', app()->getLocale())->first();

    	$data = [];
    	
    	foreach ($newstypes as $newstype) {
    		$az = $newstype->name_az;
    		$en = $newstype->name_en;
    		$ru = $newstype->name_ru;
    		$arr['key'] = $newstype->id_news_type;
    		$arr['text'] = ${app()->getLocale()};
    		$data[] = $arr;
    	}

    	$list = $this->getNewsList();

        return view('frontend/app', ["types"=>$data, 'settings' => $settings->content, 'list' => $list]);
    }

    public function broadcast()
    {
    	return view('frontend/broadcast');
    }

    public function aboutus()
    {
    	return view('frontend/aboutus');
    }

    public function setLocale(Request $request, $locale){
    	if(!in_array($locale, ['az', 'ru'])){
    		abort(404);
    	}
    	$request->session()->put('locale', $locale);
    	return redirect()->back();
    }

    private function getNewsList()
    {
    	$list = [];

    	$newslist = News::where('is_active', '=', 1)
						->where('language', '=', app()->getLocale())
						->limit(20)
						->orderBy('created_at', 'desc')
						->get();

    	foreach ($newslist as $item) {
			$arr = [];
    		$arr['title'] = $item->title;
    		$arr['slug'] = $item->slug;
    		$arr['description'] = $item->description;
			$arr['created_at'] = $item->created_at;
			$list[] = $arr;
    	}

    	return $list;
    }
}
