<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use Validator;
use App\Backend\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getAll(Request $request){
    	if(!Auth::user()->is_superadmin){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}


    	$r_all = $request->all();

        if(!array_key_exists('search', $r_all)){
            abort(404);
        }

        $availableSearch = ['role', 'updated_at', 'created_at'];
        if(!in_array($r_all['searchBy'], $availableSearch)){
            return response()->json([]);
        }

    	$permissions = Permission::where($r_all['searchBy'], 'like', '%'.$r_all["search"].'%')
                ->orderBy($r_all['orderBy'], $r_all['orderDir'])
                ->paginate((int)$r_all['limit']);

        $result = [
            "items" => $permissions->items(),
            "pager" => [
                "currentPage" => $permissions->currentPage(),
                "currentLimit" => $permissions->perPage(),
                "totalPages" => $permissions->lastPage(),
                "totalItems" => $permissions->total(),
            ]
        ];

        return response()->json($result);
    }

    public function get($id_permission){
    	if(!Auth::user()->is_superadmin){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$permission = Permission::where('id_permission', $id_permission)->first();

    	if(empty($permission)){
    		$result = [
                'haserror' => true,
                'message' => 'warning.empty_data',
            ];
    	}else{
    		$allowed = !empty(json_decode($permission->allowed, true)) ? json_decode($permission->allowed, true) : ["holder" => true];
    		$result = [
	            "item" => $permission,
	            "allowed" => $allowed
	        ];
    	}

    	return response()->json($result);
    }

    public function update(Request $request){
    	if(!Auth::user()->is_superadmin){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$permission = Permission::find($request->id_permission);

    	if(empty($permission)){
    		$result = [
                'haserror' => true,
                'message' => 'warning.empty_data',
            ];
            return response()->json($result);
    	}

    	$items = $request->data;

    	foreach ($items as $key => $value) {
    		if($value === false){
    			unset($items[$key]);
    		}
    	}

    	if(empty($items)){
    		$items["holder"] = true;
    	}

        $permission->allowed = json_encode($items);
        $permission->save();

	    $result = [
            "success" => "success.update"
        ];
	    return response()->json($result);
    }
}
