<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use App\Types;
use App\Content;
use Carbon\Carbon;
use App\Backend\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getData()
    {
    	if(!$this->hasAccess(Auth::user(), "dashboard")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$types = Types::all()->count();

    	$content_c = Content::all()->count();
    	$content_active_c = Content::where('is_active', 1)->count();
    	$content_az_c = Content::where('language', 'az')->count();

    	$adminc = Admin::all()->count();
    	$admin_active_c = Admin::where('blocked', 0)->count();

    	$contentdata = [
    		"data" => $content_c,
    		"active" => $content_active_c,
    		"total_az" => $content_az_c,
    		"total_ru" => $content_c - $content_az_c
    	];

    	$admindata = [
    		"total" => $adminc,
    		"active" => $admin_active_c,
    	];

    	$result = [
            "content" => $contentdata,
            "admin" => $admindata
        ];
        return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
    }
}