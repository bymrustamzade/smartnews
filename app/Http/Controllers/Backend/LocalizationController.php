<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocalizationController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getLocalizations(Request $request){
    	if(!$this->hasAccess(Auth::user(), "localization")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$filenames = [];

    	foreach(glob(base_path() . '/resources/js/lang/*.*') as $file) {
    		$file_arr = explode('/', $file);
    		$file = end($file_arr);
    		$name = explode('.', $file);
    		array_push($filenames, ['name' => $name[0], 'file' => $file]);
		}

		if(empty($filenames)){
    		$result = [
                'haserror' => true,
                'message' => 'warning.empty_data',
            ];
            return response()->json($result);
    	}

    	$result = [
            "items" => $filenames
        ];

        return response()->json($result);
    }

    public function getSections($position, $lang){
    	if(!$this->hasAccess(Auth::user(), "localization-edit")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	if(!in_array($position, ['backend', 'frontend']) || !in_array($lang, ['az', 'ru'])){
    		$result = [
                'haserror' => true,
                'message' => 'warning.empty_data',
            ];
            return response()->json($result);
    	}

    	if($position == 'backend'){
    		$folder = '/js/lang/';
    	}else{
    		$folder = '/lang/';
    	}

    	$json = file_get_contents(base_path() . '/resources' . $folder .$lang.'.json');
    	$json_data = json_decode($json, true);

    	$result = [
            "item" => $json_data
        ];
    	return response()->json($result);
    }

    public function update(Request $request){
    	if(!$this->hasAccess(Auth::user(), "localization-edit")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied_action',
            ];
            return response()->json($result);
    	}

    	if(!in_array($request->position, ['backend', 'frontend']) 
    		|| !in_array($request->lang, ['az', 'ru'])){
    		$result = [
                'haserror' => true,
                'message' => 'warning.empty_data',
            ];
            return response()->json($result);
    	}

    	if($request->position == 'backend'){
    		$filename = base_path() . '/resources/js/lang/'.$request->lang.'.json';
    	}else{
    		$filename = base_path() . '/resources/lang/'.$request->lang.'.json';
    	}

    	if(!file_exists($filename)){
    		$result = [
                'haserror' => true,
                'message' => 'warning.empty_data',
            ];
            return response()->json($result);
    	}

    	$data = $request->data;

    	if(file_put_contents($filename, json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT))){
            $result = [
	            "success" => "success.update"
	        ];
    	}else{
    		$result = [
                'haserror' => true,
                'message' => 'error.empty_data',
            ];
    	}
	    return response()->json($result);
    }
}
