<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use Validator;
use App\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getAll(Request $request)
    {
    	if(!$this->hasAccess(Auth::user(), "settings")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$table = DB::table('settings')
                ->paginate((int)$request->limit);

        $result = [
            "items" => $table->items(),
            "pager" => [
                "currentPage" => $table->currentPage(),
                "currentLimit" => $table->perPage(),
                "totalPages" => $table->lastPage(),
                "totalItems" => $table->total(),
            ]
        ];

        return response()->json($result);
    }

    public function get($id_settings)
    {
    	if(!$this->hasAccess(Auth::user(), "settings-edit")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$table = Settings::find($id_settings);

    	if(empty($table)){
    		$result = [
                'haserror' => true,
                'message' => 'warning.empty_data',
            ];
    	}else{
    		$content = empty(json_decode($table->content, true)) ? ["holder" => true] : json_decode($table->content, true);

    		$result = [
	            "item" => $table,
	            "content" => $content
	        ];
    	}

        return response()->json($result);
    }

    public function update(Request $request)
    {
    	if(!$this->hasAccess(Auth::user(), "settings-edit")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$table = Settings::find($request->id_settings);

    	if(empty($table)){
    		$result = [
                'haserror' => true,
                'message' => 'warning.empty_data',
            ];
            return response()->json($result);
    	}

    	$validator = Validator::make($request->content, [
            'about_us' => 'required',
            'contact' => 'required',
            'footer' => 'required',
            'broadcast' => 'required'
        ]);

        $errors = $validator->errors()->messages();

        if ($validator->fails()) {
            $error_keys = [];

            foreach ($errors as $key => $value) {
                array_push($error_keys, $key);
            }

            $result = [
                "haserror" => true,
                "message" => "warning.fill_all_fields",
                "keys" => $error_keys,
                "errors" => $errors
            ];
            return response()->json($result);
        }

        $table->content = json_encode($request->content);
        $table->save();

        $result = [
            "success" => "success.update"
        ];
	    return response()->json($result);

    }
}
