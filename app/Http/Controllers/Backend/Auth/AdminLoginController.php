<?php

namespace App\Http\Controllers\Backend\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminLoginController extends Controller
{
    public function __construct()
    {
    	$this->middleware('guest:admin');
    }

    public function showLoginForm()
    {
      return view('backend.auth.admin-login');
    }

    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);

      // Attempt to log the user in
      if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password, 'blocked' => 0], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('admin'));
      }
      
      // if unsuccessful, then redirect back to the login with the form data
      $errors = ['email' => trans('auth.failed'), 'password' => trans('auth.failed')];
      return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors($errors);
    }
}
