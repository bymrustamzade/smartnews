<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use Validator;
use App\Backend\Email;
use App\Mail\Email as Mailer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\ImageController;

class EmailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getAll(Request $request)
    {
    	if(!$this->hasAccess(Auth::user(), "emails")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}
    	$r_all = $request->all();

        if(!array_key_exists('search', $r_all)){
            abort(404);
        }

        $availableSearch = ['email', 'title', 'created_at'];
        if(!in_array($r_all['searchBy'], $availableSearch)){
            return response()->json([]);
        }

    	$news = Email::where($r_all['searchBy'], 'like', '%'.$r_all["search"].'%')
                ->orderBy($r_all['orderBy'], $r_all['orderDir'])
                ->paginate((int)$r_all['limit']);

        $result = [
            "items" => $news->items(),
            "pager" => [
                "currentPage" => $news->currentPage(),
                "currentLimit" => $news->perPage(),
                "totalPages" => $news->lastPage(),
                "totalItems" => $news->total(),
            ]
        ];

        return response()->json($result);
    }

    public function send(Request $request)
    {
    	if(!$this->hasAccess(Auth::user(), "email-send")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied_action',
            ];
            return response()->json($result);
    	}

    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'title' => 'required|min:3',
            'text' => 'required'
        ]);

        $errors = $validator->errors()->messages();

        if ($validator->fails()) {
            $error_keys = [];

            foreach ($errors as $key => $value) {
                array_push($error_keys, $key);
            }

            $result = [
                "haserror" => true,
                "message" => "warning.fill_all_fields",
                "keys" => $error_keys,
                "errors" => $errors
            ];
            return response()->json($result);
        }

        $table = new Email;
        $table->email = $request->email;
        $table->title = $request->title;
        $table->text = $request->text;
        $table->save();

        $data = [];
        $data['email'] = $table->email;
        $data['title'] = $table->title;
        $data['text'] = $table->text;
        $data['attachments'] = $request->attachments;

        \Mail::to($request->email)->send(new Mailer($data));

        $table->send = 1;
        $table->save();

        $result = [
            "success" => "success.send"
        ];
        return response()->json($result);
    }
}
