<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LanguagesController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function getAll(Request $request){
    	$contentAll = DB::table('languages')->get();

        return response()->json($contentAll);
    }

    public function setLocale(Request $request){
    	$locale = $request->get('locale');
    	$request->session()->put('locale', $locale);
    }
}
