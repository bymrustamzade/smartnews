<?php

namespace App\Http\Controllers\Backend;

use Auth;
use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\ImageController;

class GalleryController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getAll(Request $request){
    	if(!$this->hasAccess(Auth::user(), "gallery")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

        return $this->getImages('gallery', '', '', $request);
    }

    public function upload(Request $request){
    	if(!$this->hasAccess(Auth::user(), "gallery")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied_action',
            ];
            return response()->json($result);
    	}

        foreach ($request->files as $files) {
            foreach ($files as $file) {
                $file->target = $request->target;

                $image_data = new ImageData();
                $image_data->file = $file;
                $image_data->has_image_table = 'gallery';

				$img_controller = new ImageController();
                $img_controller->uploadAllImage($image_data);
            }
        }


        $result['success'] = "Uploaded successfully";
        return response()->json($result);
    }
}
