<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use Hash;
use Validator;
use App\Backend\Admin;
use App\Backend\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\ImageController;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.admin');
    }

    public function adminLogOut()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin/login');
    }

    public function getAdminRoles(){
    	if(!Auth::user()->is_superadmin){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$permissions = Permission::all();
    	$data = [
	    			['value' => -1, 'text' => 'permissions.all', 'selected' => true], 
	    			['value' => 0, 'text' => 'permissions.superadmin', 'selected' => false]
	    		];
    	foreach ($permissions as $permission) {
    		$arr['value'] = $permission->id_permission;
    		$arr['text'] = 'permissions.' . $permission->role;
    		$arr['selected'] = false;
    		$data[] = $arr;
    	}

    	$result = [
            "items" => $data
        ];
        return response()->json($result);

    }

    public function getAdmins(Request $request)
    {
    	if(!Auth::user()->is_superadmin){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$r_all = $request->all();

        if(!array_key_exists('search', $r_all)){
            abort(404);
        }

        $filterCond = '=';

        //$r_all['filterBy'] -1 is for all and 0 is for superadmin

        if($r_all['filterBy'] == -1){
        	$filterCond = '!=';
        }

        $availableSearch = ['name', 'email', 'created_at'];
        if(!in_array($r_all['searchBy'], $availableSearch)){
            return response()->json([]);
        }

        if($r_all['filterBy'] == 0){
        	$admins = Admin::where($r_all['searchBy'], 'like', '%'.$r_all["search"].'%')
    			->where('is_superadmin', '=', 1)
                ->orderBy($r_all['orderBy'], $r_all['orderDir'])
                ->paginate((int)$r_all['limit']);
        }else{
        	$admins = Admin::where($r_all['searchBy'], 'like', '%'.$r_all["search"].'%')
    			->where('role', $filterCond, $r_all['filterBy'])
                ->orderBy($r_all['orderBy'], $r_all['orderDir'])
                ->paginate((int)$r_all['limit']);
        }

        $items = [];

        foreach ($admins as $admin) {
        	$arr['name'] = $admin->name;
        	$arr['email'] = $admin->email;
        	$arr['id_admin'] = $admin->id_admin;
        	$arr['is_superadmin'] = $admin->is_superadmin;
        	$arr['created_at'] = $admin->created_at;
        	$arr['updated_at'] = $admin->updated_at;
        	$arr['role'] = $admin->permissions->role;
        	$arr['image'] = $admin->images->where('is_main', '=', 1)->first();
        	$items[] = $arr;
        }

        $result = [
            'items' => $items,
            'pager' => [
                'currentPage' => $admins->currentPage(),
                'currentLimit' => $admins->perPage(),
                'totalPages' => $admins->lastPage(),
                'totalItems' => $admins->total(),
            ]
        ];

        return response()->json($result);

    }

    public function get($id_admin)
    {
    	if(!Auth::user()->is_superadmin){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$admin = Admin::where('id_admin', $id_admin)->first();
    	$data = null;
    	if(!empty($admin)){
    		$data['id_admin'] = $admin->id_admin;
	    	$data['name'] = $admin->name;
	    	$data['email'] = $admin->email;
	    	$data['role'] = $admin->role;
	    	$data['is_superadmin'] = $admin->is_superadmin;
	    	$data['blocked'] = $admin->blocked;
	    	$allowed = !empty(json_decode($admin->admin_allowed, true)) ? json_decode($admin->admin_allowed, true) : ["holder" => true];
	    	$data['admin_allowed'] = $allowed;
    	}
    	

        $result = [
            "item" => $data
        ];
    	return response()->json($result);

    }

    public function images(Request $request, $id_admin){
    	if(!Auth::user()->is_superadmin){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

        return $this->getImages('admin_images', 'id_admin', $id_admin, $request, 'admin_images.is_main, ');
    }


    public function add(Request $request)
    {
    	if(!Auth::user()->is_superadmin){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$validator = Validator::make($request->all(), [
			'name' => 'required|min:3|regex:/^[\pL\s\-]+$/u',
			'email' => 'required|email|unique:admins',
			'role' => 'required|exists:permissions,id_permission',
			'password' => 'required|min:8|confirmed',
	        'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
        	$errors = [];
            $error_keys = [];

            $errors = $validator->errors()->messages();

            foreach ($errors as $key => $value) {
                array_push($error_keys, $key);
            }

            $result = [
                "haserror" => true,
                "message" => "warning.fill_all_fields",
                "keys" => $error_keys,
                "errors" => $errors
            ];
            return response()->json($result);
        }

        $admin = new Admin;
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->role = $request->role;
        $admin->is_superadmin = $request->is_superadmin;
        $admin->password = Hash::make($request->password);
        $admin->save();

        $result = [
            "success" => "success.add",
            "id" => $admin->id_admin
        ];
        return response()->json($result);
    }

    public function update(Request $request)
    {
    	if(!Auth::user()->is_superadmin){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	if(!empty($data["password"])){
    		$validator = Validator::make($request->all(), [
				'name' => 'required|min:3|regex:/^[\pL\s\-]+$/u',
				'email' => 'required|email|unique:admins,email,'.$request->id_admin.',id_admin',
				'role' => 'required|exists:permissions,id_permission',
				'password' => 'required|min:8|confirmed',
		        'password_confirmation' => 'required'
	        ]);
    	}else{
    		$validator = Validator::make($request->all(), [
				'name' => 'required|min:3|regex:/^[\pL\s\-]+$/u',
				'email' => 'required|email|unique:admins,email,'.$request->id_admin.',id_admin',
				'role' => 'required|exists:permissions,id_permission'
	        ]);
    	}

    	if ($validator->fails()) {
            $error_keys = [];

            $errors = $validator->errors()->messages();

            foreach ($errors as $key => $value) {
                array_push($error_keys, $key);
            }

            $result = [
                "haserror" => true,
                "message" => "warning.fill_all_fields",
                "keys" => $error_keys,
                "errors" => $errors
            ];
            return response()->json($result);
        }

        $admin = Admin::find($request->id_admin);

        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->role = $request->role;
        $admin->is_superadmin = $request->is_superadmin;
        $admin->blocked = $request->blocked;
        if(!empty($request->password)){
        	$admin->password = Hash::make($request->password);
        }
        $admin->admin_allowed = json_encode($request->admin_allowed);
        $admin->save();

        $result = [
            "success" => "success.update"
        ];
	    return response()->json($result);
    }


    public function updateMainImage(Request $request)
    {
    	return $this->setMainImage('admin_images', 'id_admin', $request->id_admin, $request->id_image);
    }

    public function uploadImage(Request $request)
    {
    	if(!Auth::user()->is_superadmin){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

        foreach ($request->files as $files) {
            foreach ($files as $file) {
                $file->target = $request->target;

                $image_data = new ImageData();
                $image_data->file = $file;
                $image_data->has_image_table = 'admin_images';
				$image_data->table_id_name = 'id_admin';
				$image_data->id_table = $request->id_admin;
				$image_data->public = 0;

				$img_controller = new ImageController();
                $img_controller->uploadAllImage($image_data);
            }
        }


        $result['success'] = "success.upload";
        return response()->json($result);
    }
}
