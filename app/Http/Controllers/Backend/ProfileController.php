<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use Hash;
use Validator;
use App\Backend\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\ImageController;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getData()
    {
        $user = Auth::user();

        $result = [
            'data' => [
            	'name' => $user->name, 
            	'email' => $user->email, 
            	'created_at' => date('H:i Y/m/d', strtotime($user->created_at->toDateTimeString()))
            ],
            'permissions' => json_decode(Auth::user()->permissions->allowed, true),
            'admin_allowed' => json_decode(Auth::user()->admin_allowed, true),
            'is_superadmin' => Auth::user()->is_superadmin,
            'image' => Auth::user()->images->where('is_main', '=', 1)->first()
        ];

        return response()->json($result);
    }

    public function images(Request $request)
    {
    	return $this->getImages('admin_images', 'id_admin', Auth::user()->id_admin, $request, 'admin_images.is_main, ');

    }

    public function update(Request $request)
    {
    	$data = $request->all()["data"];
    	$user = Auth::user();

    	if(!empty($data["password"])){
    		$validator = Validator::make($data, [
				'email' => 'required|email|unique:admins,email,'.$user->id_admin.',id_admin',
				'name' => 'required|min:3|regex:/^[\pL\s\-]+$/u',
				'password' => 'required',
		        'password_new' => 'required|min:8|confirmed|different:password',
		        'password_new_confirmation' => 'required'
	        ]);
    	}else{
    		$validator = Validator::make($data, [
				'email' => 'required|email|unique:admins,email,'.$user->id_admin.',id_admin',
				'name' => 'required|min:3|regex:/^[\pL\s\-]+$/u'
	        ]);
    	}

    	if ($validator->fails()) {
            $error_keys = [];

            $errors = $validator->errors()->messages();

            foreach ($errors as $key => $value) {
                array_push($error_keys, $key);
            }

            $result = [
                "haserror" => true,
                "message" => "warning.fill_all_fields",
                "keys" => $error_keys,
                "errors" => $errors
            ];
            return response()->json($result);
        }

        $admin = Admin::find($user->id_admin);

        if (!empty($data["password"]) && !Hash::check($data["password"], $admin->password)) {
            $result = [
                'haserror' => true,
                'message' => 'error.wrong_password',
            ];
            return response()->json($result);
		}

        $admin->name = $data["name"];
        $admin->email = $data["email"];
        if(!empty($data["password"])){
        	$admin->password = Hash::make($data["password_new"]);
        }
        $admin->save();

        $result = [
            "success" => "success.update"
        ];
	    return response()->json($result);
    }

    public function uploadImage(Request $request){

        foreach ($request->files as $files) {
            foreach ($files as $file) {
                $file->target = $request->target;

                $image_data = new ImageData();
                $image_data->file = $file;
                $image_data->has_image_table = 'admin_images';
				$image_data->table_id_name = 'id_admin';
				$image_data->id_table = Auth::user()->id_admin;
				$image_data->public = 0;

				$img_controller = new ImageController();
                $img_controller->uploadAllImage($image_data);
            }
        }


        $result['success'] = "success.upload";
        return response()->json($result);
    }

    public function updateMainImage(Request $request)
    {
    	return $this->setMainImage('admin_images', 'id_admin', Auth::user()->id_admin, $request->id_image);
    }
}
