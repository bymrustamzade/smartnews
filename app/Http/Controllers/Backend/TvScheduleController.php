<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Validator;
use Carbon\Carbon;
use App\TvSchedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TvScheduleController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    private function prepareDates()
    {
    	$startDate = Carbon::today();
    	$endDate = Carbon::today()->add(1, 'month');
		$data = [];

		while ($startDate->lte($endDate)){
			$data[] = $startDate->format('d-m-Y');
			$startDate->addDay();
		}
		return $data;
    }

    public function getDays()
    {
    	$days = $this->prepareDates();
    	$data = [];

    	foreach ($days as $day) {
    		$arr = [];
    		$arr['day'] = $day;
			$arr['isHovering'] = false;
			$data['days'][] = $arr;
    	}

		return response()->json($data);
    }



    public function get($day, $lang)
    {
    	if(!$this->hasAccess(Auth::user(), "tvschedule-edit")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$days = $this->prepareDates();

    	$filenames = [];

    	foreach(glob(base_path() . '/resources/js/lang/*.*') as $file) {
    		$file_arr = explode('/', $file);
    		$file = end($file_arr);
    		$name = explode('.', $file);
    		array_push($filenames, $name[0]);
		}

    	if(!in_array($day, $days) || !in_array($lang, $filenames)){
    		$result = [
                'haserror' => true,
                'message' => 'error.doesnt_exists',
            ];
            return response()->json($result);
    	}


    	$nday = date_format(date_create($day), 'Y-m-d');
    	$tvschedule = TvSchedule::where('day', $nday)->where('language', $lang)->first();
    	$data = [];

    	if($tvschedule != null){
    		$data['language'] = $tvschedule->language;
    		$data['data'] = json_decode($tvschedule->data);
    	}else{
    		$data['data'] = [];
    	}

    	$result = [
            "item" => $data
        ];
    	return response()->json($result);
    }

    public function update(Request $request)
    {
    	if(!$this->hasAccess(Auth::user(), "tvschedule-edit")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$days = $this->prepareDates();

    	$day = $request->day;

    	if(!in_array($day, $days)){
    		$result = [
                'haserror' => true,
                'message' => 'error.doesnt_exists',
            ];
            return response()->json($result);
    	}

    	$validator = Validator::make($request->all(), [
    		'language' => 'required',
            'data.*.hour_start' => 'required|date_format:H:i',
            'data.*.hour_end' => 'required_with:*.hour_start|date_format:H:i',
            'data.*.description' => 'required_with_all:*.hour_start,*.hour_end|min:3|max:255'
        ]);

        if ($validator->fails()) {
        	$errors = [];
            $error_keys = [];

            $errors = $validator->errors()->messages();

            foreach ($errors as $key => $value) {
                array_push($error_keys, $key);
            }

            $result = [
                "haserror" => true,
                "message" => "warning.fill_all_fields",
                "keys" => $error_keys,
                "errors" => $errors
            ];
            return response()->json($result);
        }

    	$nday = date_format(date_create($day), 'Y-m-d');
    	$data = TvSchedule::where('day', $nday)->where('language', $request->language)->first();

    	if($data){
    		$data->data = json_encode($request->data);
    		$data->language = $request->language;
    		$data->save();
    	}else{
    		$table = new TvSchedule;
    		$table->day = $nday;
    		$table->language = $request->language;
    		$table->data = json_encode($request->data);
    		$table->save();
    	}

    	$result = [
            "success" => "success.update"
        ];
        return response()->json($result);

    }
}
