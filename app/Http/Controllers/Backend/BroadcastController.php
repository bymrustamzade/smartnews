<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use Image;
use Broadcast;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BroadcastController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function getAll(Request $request){
    	if(!$this->hasAccess(Auth::user(), "broadcast")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}
    	$r_all = $request->all();
    	
        if(!array_key_exists('search', $r_all)){
            abort(404);
        }

        $availableSearch = ['internal_name', 'name', 'height', 'created_at'];
        if(!in_array($r_all['searchBy'], $availableSearch)){
            return response()->json([]);
        }

    	$broadcasts = DB::table('broadcast')
                ->where('broadcast.' . $r_all['searchBy'], 'like', '%'.$r_all["search"].'%')
                ->where('public', '=', 1)
                ->orderBy('broadcast.' . $r_all['orderBy'], $r_all['orderDir'])
                ->paginate((int)$r_all['limit']);

        $result = [
            "items" => $broadcasts->items(),
            "pager" => [
                "currentPage" => $broadcasts->currentPage(),
                "currentLimit" => $broadcasts->perPage(),
                "totalPages" => $broadcasts->lastPage(),
                "totalItems" => $broadcasts->total(),
            ]
        ];

        return response()->json($result);
    }

    public function upload(Request $request){
    	if(!$this->hasAccess(Auth::user(), "broadcasts")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

        foreach ($request->files as $files) {
            foreach ($files as $file) {
                $file->target = $request->target;

                $broadcast_data = new VideoData();
                $broadcast_data->file = $file;

                $this->uploadAllBroadcast($broadcast_data);
            }
        }

        $result['success'] = "Uploaded successfully";
        return response()->json($result);
    }

    public function uploadAllBroadcast(VideoData $broadcast_data){
    	if(!$this->hasAccess(Auth::user(), "broadcasts")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}
        $internal_name = Carbon::now()->timestamp . '_' . uniqid() . '.' . $broadcast_data->file->getClientOriginalExtension();
        $filename = str_replace("." . $broadcast_data->file->getClientOriginalExtension(),
        						"",
        						$broadcast_data->file->getClientOriginalName()
        			);

        $target = $broadcast_data->file->target;

        if (!file_exists(public_path('/storage/broadcast/default/' . $target))) {
            mkdir(public_path('/storage/broadcast/default/' . $target), 0777, true);
        }

        ini_set('memory_limit','256M');

        $video_size = $this->formatBytes($broadcast_data->file->getSize());
        $moved = $broadcast_data->file->move(public_path('/storage/broadcast/default/' . $target), $internal_name);

        if(!$moved){
        	return false;
        }

        $id_broadcast = DB::table('broadcast')
                    ->insertGetId([
                        'name' => $filename,
                        'internal_name' => $internal_name,
                        'path' => '/storage/broadcast/default/' . $target.'/',
                        'filesize' => $video_size,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);

        if($broadcast_data->has_broadcast_table != null){
        	$table = DB::table($broadcast_data->has_broadcast_table);
        	
        	if($broadcast_data->table_id_name != null){
                $table->insert([
                    $broadcast_data->table_id_name => $broadcast_data->id_table,
                    'id_broadcast' => $id_broadcast,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
        	}else{
            	$table->insert([
                    'id_broadcast' => $id_broadcast,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
        	}
            

            if(property_exists($broadcast_data, 'public') && $broadcast_data->public === 0){
            	DB::table('broadcast')->where('id_broadcast', $id_broadcast)->update(['public' => 0]);
            }
        }

        return true;
    }

    public function deleteVideo(Request $request){
    	if(!$this->hasAccess(Auth::user(), "broadcast-delete")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied_action',
            ];
            return response()->json($result);
    	}
        $r_all = $request->all();

        $broadcast = DB::table('broadcast')
                ->where('id_broadcast', $r_all['id_broadcast'])
                ->first();

        if (file_exists(public_path($broadcast->path . $broadcast->internal_name))) {
            unlink(public_path($broadcast->path . $broadcast->internal_name));
        }

        $d_broadcast = DB::table('broadcast')
                    ->where('id_broadcast', $r_all['id_broadcast'])
                    ->delete();

        $result['success'] = "deleted successfully";
        return response()->json($result);
    }

    private function formatBytes($bytes, $precision = 2) { 
	    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

	    $bytes = max($bytes, 0); 
	    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
	    $pow = min($pow, count($units) - 1); 

	    $bytes /= pow(1024, $pow); 

	    return round($bytes, $precision); 
	} 
}

class VideoData {
	public $file;
	public $has_broadcast_table = null;
	public $table_id_name = null;
	public $id_table = null;
}
