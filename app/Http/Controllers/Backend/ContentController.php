<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use Validator;
use App\Types;
use App\Content;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\ImageController;

class ContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getTypes()
    {
    	if(!$this->hasAccess(Auth::user(), "content")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$types = Types::get();
    	$data = [
	    			['value' => 0, 'text' => 'content.all', 'selected' => true]
	    		];
    	foreach ($types as $type) {
    		$az = $type->name_az;
    		$en = $type->name_en;
    		$ru = $type->name_ru;
    		$arr['value'] = $type->id_type;
    		$arr['text'] = ${app()->getLocale()};
    		$arr['selected'] = false;
    		$arr['non_translate'] = true;
    		$data[] = $arr;
    	}

    	$result = [
            "items" => $data
        ];
        return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
    }
    
    public function getAll(Request $request){
    	if(!$this->hasAccess(Auth::user(), "content")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}
    	$r_all = $request->all();

        if(!array_key_exists('search', $r_all)){
            abort(404);
        }

        $filterCond = '=';

        //$r_all['filterBy'] 0 is for all

        if($r_all['filterBy'] == 0){
        	$filterCond = '!=';
        }

        $availableSearch = ['internal_name', 'title', 'slug', 'language', 'created_at'];
        if(!in_array($r_all['searchBy'], $availableSearch)){
            return response()->json([]);
        }

    	$content = Content::where($r_all['searchBy'], 'like', '%'.$r_all["search"].'%')
    			->where('id_type', $filterCond, $r_all['filterBy'])
                ->orderBy($r_all['orderBy'], $r_all['orderDir'])
                ->paginate((int)$r_all['limit']);

        $items = [];

        foreach ($content as $c) {
        	$arrvar = ["az" => "name_az", "en" => "name_en", "ru" => "name_ru"];
        	$arr['id_content'] = $c->id_content;
        	$arr['internal_name'] = $c->internal_name;
        	$arr['id_type'] = $c->id_type;
        	$arr['type'] = $c->types->{$arrvar[app()->getLocale()]};
        	$arr['title'] = $c->title;
        	$arr['slug'] = $c->slug;
        	$arr['language'] = $c->language;
        	$arr['is_active'] = $c->is_active;
        	$arr['created_at'] = $c->created_at;
        	$arr['updated_at'] = $c->updated_at;
        	$items[] = $arr;
        }

        $result = [
            "items" => $items,
            "pager" => [
                "currentPage" => $content->currentPage(),
                "currentLimit" => $content->perPage(),
                "totalPages" => $content->lastPage(),
                "totalItems" => $content->total(),
            ]
        ];

        return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    public function get($id_content){
    	if(!$this->hasAccess(Auth::user(), "content-edit")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$content = Content::where('id_content', $id_content)->first();

    	if(empty($content)){
    		$result = [
                'haserror' => true,
                'message' => 'warning.empty_data',
            ];
            return response()->json($result);
    	}

    	if($content){
    		$content->type = $content->id_type;
    	}

        $result = [
            "item" => $content
        ];
    	return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    public function images(Request $request, $id_content){
    	if(!$this->hasAccess(Auth::user(), "content-edit")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

        return $this->getImages('content_images', 'id_content', $id_content, $request, 'content_images.is_main, ');
    }

    public function add(Request $request){
    	if(!$this->hasAccess(Auth::user(), "content-add")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied_action',
            ];
            return response()->json($result);
    	}

        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'internal_name' => 'required|min:3|max:255|unique:content',
            'language' => 'required',
            'title' => 'required|min:3|max:255',
            'slug' => 'required|min:3|max:255|unique:content',
            'topics' => ['max:255', 'regex:/^[\p{L}\d\s\,-]+$/u'],
            'description' => 'required',
            'body' => 'required',
            'seo_title' => 'max:255',
            'seo_keywords' => 'max:255',
            'seo_description' => 'max:255',
            'seo_robots' => 'max:255'
        ]);

        if ($validator->fails()) {
        	$errors = [];
            $error_keys = [];

            $errors = $validator->errors()->messages();

            foreach ($errors as $key => $value) {
                array_push($error_keys, $key);
            }

            $result = [
                "haserror" => true,
                "message" => "warning.fill_all_fields",
                "keys" => $error_keys,
                "errors" => $errors
            ];
            return response()->json($result);
        }

        $table = new Content;
        $table->id_type = $request->type;
        $table->internal_name = $request->internal_name;
        $table->language = $request->language;
        $table->title = $request->title;
        $table->slug = $request->slug;
        $table->topics = rtrim($request->topics,',');
        $table->description = $request->description;
        $table->body = $request->body;
        $table->seo_title = $request->seo_title;
        $table->seo_description = $request->seo_description;
        $table->seo_keywords = $request->seo_keywords;
        $table->seo_robots = $request->seo_robots;
        $table->is_active = (int)$request->is_active;
        $table->is_paid = (int)$request->is_paid;
        $table->is_big = (int)$request->is_big;
        $table->save();

        $result = [
            "success" => "success.add",
            "id" => $table->id_content
        ];
        return response()->json($result);

    }

    public function update(Request $request){
    	if(!$this->hasAccess(Auth::user(), "content-edit")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied_action',
            ];
            return response()->json($result);
    	}
    	$table = Content::where('id_content', $request->id_content)->first();
    	if(empty($table)){
    		$result = [
                'haserror' => true,
                'message' => 'warning.empty_data',
            ];
            return response()->json($result);
    	}

        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'internal_name' => 'required|min:3|max:255|unique:content,internal_name,' . $request->id_content . ',id_content',
            'language' => 'required',
            'title' => 'required|min:3|max:255',
            'slug' => 'required|min:3|max:255|unique:content,slug,' . $request->id_content . ',id_content',
            'topics' => 'max:255|regex:/^[\p{L}\d\s\,-]+$/u',
            'description' => 'required',
            'body' => 'required',
            'seo_title' => 'max:255',
            'seo_keywords' => 'max:255',
            'seo_description' => 'max:255',
            'seo_robots' => 'max:255'
        ]);

        $errors = $validator->errors()->messages();

        if ($validator->fails()) {
            $error_keys = [];

            foreach ($errors as $key => $value) {
                array_push($error_keys, $key);
            }

            $result = [
                "haserror" => true,
                "message" => "warning.fill_all_fields",
                "keys" => $error_keys,
                "errors" => $errors
            ];
            return response()->json($result);
        }

        $table->id_type = $request->type;
        $table->internal_name = $request->internal_name;
        $table->language = $request->language;
        $table->title = $request->title;
        $table->slug = $request->slug;
        $table->topics = rtrim($request->topics,',');
        $table->description = $request->description;
        $table->body = $request->body;
        $table->seo_title = $request->seo_title;
        $table->seo_description = $request->seo_description;
        $table->seo_keywords = $request->seo_keywords;
        $table->seo_robots = $request->seo_robots;
        $table->is_active = (int)$request->is_active;
        $table->is_paid = (int)$request->is_paid;
        $table->is_big = (int)$request->is_big;
        $table->updated_at = Carbon::now()->toDateTimeString();
        $table->save();

	    $result = [
            "success" => "success.update"
        ];
	    return response()->json($result);
    }

    public function uploadImage(Request $request){
    	if(!$this->hasAccess(Auth::user(), "content-edit")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied_action',
            ];
            return response()->json($result);
    	}
    	$table = DB::table('content')
                ->where('id_content', $request->id_content)
                ->first();

        if(empty($table)){
        	$result = [
		                "haserror" => true,
		                "message" => 'error.upload',
		            ];
            return response()->json($result);
        }


        foreach ($request->files as $files) {
            foreach ($files as $file) {
                $file->target = $request->target;

                $image_data = new ImageData();
                $image_data->file = $file;
                $image_data->has_image_table = 'content_images';
				$image_data->table_id_name = 'id_content';
				$image_data->id_table = $table->id_content;

				$img_controller = new ImageController();
                $img_controller->uploadAllImage($image_data);
            }
        }


        $result['success'] = "Uploaded successfully";
        return response()->json($result);
    }

    public function updateMainImage(Request $request)
    {
    	return $this->setMainImage('content_images', 'id_content', $request->id_content, $request->id_image);
    }

    public function delete(Request $request){
    	if(!$this->hasAccess(Auth::user(), "content-delete")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied_action',
            ];
            return response()->json($result);
    	}

    	$delete_images = DB::table('content_images')
                        ->where('id_content', $request->id_content)
                        ->delete();

    	$delete_item = DB::table('content')
                        ->where('id_content', $request->id_content)
                        ->delete();

        $result['success'] = "deleted successfully";
        return response()->json($result);
    }
}
