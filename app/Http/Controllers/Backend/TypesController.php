<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use Validator;
use App\Types;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\ImageController;

class TypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function getAll(Request $request){
    	if(!$this->hasAccess(Auth::user(), "types")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}
    	$r_all = $request->all();

        if(!array_key_exists('search', $r_all)){
            abort(404);
        }

        $filterCond = '=';

        //$r_all['filterBy'] 0 is for all

        if(is_numeric($r_all['filterBy'])){
        	$filterCond = '!=';
        }


        $availableSearch = ['type', 'internal_name', 'name_az', 'name_en', 'name_ru', 'created_at'];
        if(!in_array($r_all['searchBy'], $availableSearch)){
            return response()->json([]);
        }

    	$types = Types::where($r_all['searchBy'], 'like', '%'.$r_all["search"].'%')
    			->where('type', $filterCond, $r_all['filterBy'])
                ->orderBy($r_all['orderBy'], $r_all['orderDir'])
                ->paginate((int)$r_all['limit']);

        $result = [
            "items" => $types->items(),
            "pager" => [
                "currentPage" => $types->currentPage(),
                "currentLimit" => $types->perPage(),
                "totalPages" => $types->lastPage(),
                "totalItems" => $types->total(),
            ]
        ];

        return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
    }

    public function get($id_type){
    	if(!$this->hasAccess(Auth::user(), "types-edit")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}

    	$types = Types::where('id_type', $id_type)->first();

        $result = [
            "item" => $types
        ];
    	return response()->json($result);
    }

    public function add(Request $request){
    	if(!$this->hasAccess(Auth::user(), "types-add")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied_action',
            ];
            return response()->json($result);
    	}
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'internal_name' => 'required|min:3|unique:types',
            'name_az' => 'required|min:3',
            'name_en' => 'required|min:3',
            'name_ru' => 'required|min:3'
        ]);

        if ($validator->fails()) {
        	$errors = [];
            $error_keys = [];

            $errors = $validator->errors()->messages();

            foreach ($errors as $key => $value) {
                array_push($error_keys, $key);
            }

            $result = [
                "haserror" => true,
                "message" => "warning.fill_all_fields",
                "keys" => $error_keys,
                "errors" => $errors
            ];
            return response()->json($result);
        }

        $type = new Types;
        $type->type = $request->type;
        $type->internal_name = $request->internal_name;
        $type->name_az = $request->name_az;
        $type->name_ru = $request->name_ru;
        $type->name_en = $request->name_en;
        $type->sort = $request->sort;
        $type->is_active = $request->is_active == 1;
        $type->save();

        $result = [
            "success" => "success.add",
            "id" => $type->id_type
        ];
        return response()->json($result, 200, [], JSON_NUMERIC_CHECK);

    }

    public function update(Request $request){
    	if(!$this->hasAccess(Auth::user(), "types-edit")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied_action',
            ];
            return response()->json($result);
    	}
    	$type = Types::where('id_type', $request->id_type)->first();
    	if(empty($type)){
    		$result = [
                'haserror' => true,
                'message' => 'warning.empty_data',
            ];
            return response()->json($result);
    	}

        $validator = Validator::make($request->all(), [
            'internal_name' => 'required|min:3|unique:types,internal_name,' . $request->id_type . ',id_type',
            'name_az' => 'required|min:3',
            'name_en' => 'required|min:3',
            'name_ru' => 'required|min:3',
            'sort' => 'integer|min:0'
        ]);

        $errors = $validator->errors()->messages();

        if ($validator->fails()) {
            $error_keys = [];

            foreach ($errors as $key => $value) {
                array_push($error_keys, $key);
            }

            $result = [
                "haserror" => true,
                "message" => "warning.fill_all_fields",
                "keys" => $error_keys,
                "errors" => $errors
            ];
            return response()->json($result);
        }

        $type->internal_name = $request->internal_name;
        $type->name_az = $request->name_az;
        $type->name_ru = $request->name_ru;
        $type->name_en = $request->name_en;
        $type->sort = $request->sort;
        $type->is_active = $request->is_active == 1;
        $type->save();

	    $result = [
            "success" => "success.update"
        ];
	    return response()->json($result);
    }

    public function getTypes(){
    	$types = Types::get();
    	$data = [
	    			['value' => 0, 'text' => 'types.all', 'selected' => true]
	    		];
    	foreach ($types as $type) {
    		$az = $type->name_az;
    		$en = $type->name_en;
    		$ru = $type->name_ru;
    		$arr['value'] = $type->id_type;
    		$arr['text'] = ${app()->getLocale()};
    		$arr['selected'] = false;
    		$arr['non_translate'] = true;
    		$data[] = $arr;
    	}

    	$result = [
            "items" => $data
        ];
        return response()->json($result, 200, [], JSON_NUMERIC_CHECK);
    }
}
