<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use Image;
use Carbon\Carbon;
use ImageOptimizer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function getAll(Request $request){
    	if(!$this->hasAccess(Auth::user(), "images")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}
    	$r_all = $request->all();
    	
        if(!array_key_exists('search', $r_all)){
            abort(404);
        }

        $availableSearch = ['filename', 'path', 'width', 'height', 'created_at'];
        if(!in_array($r_all['searchBy'], $availableSearch)){
            return response()->json([]);
        }

    	$images = DB::table('image')
                ->where('image.' . $r_all['searchBy'], 'like', '%'.$r_all["search"].'%')
                ->where('public', '=', 1)
                ->selectRaw('image.*, 
                            image_thumbnail.filename as t_filename, 
                            image_thumbnail.path as t_path,
                            image_thumbnail.width as t_width,
                            image_thumbnail.height as t_height'
                            )
                ->leftJoin('image_thumbnail', 'image_thumbnail.id_image', '=', 'image.id_image')
                ->orderBy('image.' . $r_all['orderBy'], $r_all['orderDir'])
                ->paginate((int)$r_all['limit']);

        $result = [
            "items" => $images->items(),
            "pager" => [
                "currentPage" => $images->currentPage(),
                "currentLimit" => $images->perPage(),
                "totalPages" => $images->lastPage(),
                "totalItems" => $images->total(),
            ]
        ];

        return response()->json($result);
    }

    

    public function getThumbnail($id_image){
    	if(!$this->hasAccess(Auth::user(), "images")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}
        $image = DB::table('image_thumbnail')
                ->where('id_image', $id_image)
                ->first();

        $url['image'] = $image->path . $image->filename;

        return response()->json($url);
    }

    public function upload(Request $request){
    	if(!$this->hasAccess(Auth::user(), "images")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}
        foreach ($request->files as $files) {
            foreach ($files as $file) {
                $file->target = $request->target;

                $image_data = new ImageData();
                $image_data->file = $file;

                $this->uploadAllImage($image_data);
            }
        }

        $result['success'] = "Uploaded successfully";
        return response()->json($result);
    }

    public function uploadAllImage(ImageData $image_data){
    	if(!$this->hasAccess(Auth::user(), "images")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied',
            ];
            return response()->json($result);
    	}
        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.jpg';

        $target = $image_data->file->target;

        if (!file_exists(public_path('/storage/default/' . $target))) {
            mkdir(public_path('/storage/default/' . $target), 0777, true);
            mkdir(public_path('/storage/default/' . $target . '/300'), 0777, true);
        }

        if (!file_exists(public_path('/storage/default/' . $target . '/300'))) {
            mkdir(public_path('/storage/default/' . $target . '/300'), 0777, true);
        }

        if (!file_exists(public_path('/storage/thumbnails/' . $target))) {
            mkdir(public_path('/storage/thumbnails/' . $target), 0777, true);
        }

        ini_set('memory_limit','256M');

        $image = Image::make($image_data->file)->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->encode('jpg', 20)->save(public_path('/storage/default/' . $target).'/'.$fileName);

        $imagesmall = Image::make($image_data->file)->resize(400, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->encode('jpg', 20)->save(public_path('/storage/default/' . $target).'/300/'.$fileName);


        $thumbnail =Image::make($image_data->file)->resize(100, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->encode('jpg', 20)->save(public_path('/storage/thumbnails/' . $target).'/'.$fileName);

        $id_image = DB::table('image')
                    ->insertGetId([
                        'filename' => $fileName,
                        'path' => '/storage/default/' . $target.'/',
                        'width' => $image->width(),
                        'height' => $image->height(),
                        'filesize' => $image->filesize(),
                        'created_at' => date('Y-m-d H:i:s')
                    ]);

        $id_thumbnail = DB::table('image_thumbnail')
                    ->insertGetId([
                        'id_image' => $id_image,
                        'filename' => $fileName,
                        'path' => '/storage/thumbnails/' . $target.'/',
                        'width' => $thumbnail->width(),
                        'height' => $thumbnail->height(),
                        'filesize' => $thumbnail->filesize(),
                        'created_at' => date('Y-m-d H:i:s')
                    ]);

        if($image_data->has_image_table != null){
        	$table = DB::table($image_data->has_image_table);
        	
        	if($image_data->table_id_name != null){
                $table->insert([
                    $image_data->table_id_name => $image_data->id_table,
                    'id_image' => $id_image,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
        	}else{
            	$table->insert([
                    'id_image' => $id_image,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
        	}
            

            if(property_exists($image_data, 'public') && $image_data->public === 0){
            	DB::table('image')->where('id_image', $id_image)->update(['public' => 0]);
            }
        }

        return true;
    }

    public function deleteImage(Request $request){
    	if(!$this->hasAccess(Auth::user(), "image-delete")){
    		$result = [
                'haserror' => true,
                'message' => 'warning.access_denied_action',
            ];
            return response()->json($result);
    	}
        $r_all = $request->all();

        $image = DB::table('image')
                ->where('id_image', $r_all['id_image'])
                ->first();

        $image_thumbnail = DB::table('image_thumbnail')
                ->where('id_image', $r_all['id_image'])
                ->first();


        if (file_exists(public_path($image->path . $image->filename))) {
            unlink(public_path($image->path . $image->filename));
        }

        if (file_exists(public_path($image->path . '300/' . $image->filename))) {
            unlink(public_path($image->path . '300/' . $image->filename));
        }

        if (file_exists(public_path($image_thumbnail->path . $image_thumbnail->filename))) {
            unlink(public_path($image_thumbnail->path . $image_thumbnail->filename));
        }

        $d_adminImage = DB::table('admin_images')
                        ->where('id_image', $r_all['id_image'])
                        ->delete();
        $d_contentImage = DB::table('content_images')
                        ->where('id_image', $r_all['id_image'])
                        ->delete();
        $d_gallery = DB::table('gallery')
                        ->where('id_image', $r_all['id_image'])
                        ->delete();

        $d_thumbnail = DB::table('image_thumbnail')
                        ->where('id_image', $r_all['id_image'])
                        ->delete();

        $d_image = DB::table('image')
                    ->where('id_image', $r_all['id_image'])
                    ->delete();

        $result['success'] = "deleted successfully";
        return response()->json($result);
    }
}

class ImageData {
	public $file;
	public $has_image_table = null;
	public $table_id_name = null;
	public $id_table = null;
}
