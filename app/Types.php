<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Types extends Model
{
    use Notifiable;

    protected $table = "types";
    protected $primaryKey = 'id_type';

    public function content()
	{
		return $this->hasMany(Content::class, 'id_type');
	}
}
