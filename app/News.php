<?php

namespace App;

use App\Libs\Image;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
	protected $primaryKey = 'id_news';
    protected $table = 'news';


    public function images(){
        return $this->belongsToMany(Image::class, 'news_images', 'id_news', 'id_image')->select('image.*','news_images.is_main');
    }

    public function types(){
    	return $this->belongsTo(AllTypes::class, 'id_alltype');
    }
}
