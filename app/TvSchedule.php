<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TvSchedule extends Model
{
    protected $primaryKey = 'id_tv_schedule';
    protected $table = 'tv_schedule';
}
