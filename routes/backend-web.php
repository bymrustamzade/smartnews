<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('get')->group(function(){
	//Admin calls
	Route::get('/admins', 'AdminController@getAdmins')->name('get-admins');
	Route::get('/admin-roles', 'AdminController@getAdminRoles')->name('get-admin-roles');
	Route::get('/admin/{id_admin}', 'AdminController@get')->name('get-admin-item');
	Route::get('/admin-images/{id_admin}', 'AdminController@images')->name('get-admin-images');
	
	//Profile calls
	Route::get('/profile-images', 'ProfileController@images')->name('get-profile-images');
	Route::get('/profile-data', 'ProfileController@getData')->name('get-profile-data');	

	//Language calls
	Route::get('/languages', 'LanguagesController@getAll')->name('get-languages');

	//Image calls
	Route::get('/images', 'ImageController@getAll')->name('get-image-items');
	Route::get('/image-thumbnail/{id_image}', 'ImageController@getThumbnail')->name('get-image-thumbnail');
	//Broadcast calls
	Route::get('/broadcasts', 'BroadcastController@getAll')->name('get-broadcast-items');
	Route::get('/broadcast-thumbnail/{id_broadcast}', 'BroadcastController@getThumbnail')->name('get-broadcast-thumbnail');

	//Localization calls
	Route::get('/localizations', 'LocalizationController@getLocalizations')->name('get-localizations');
	Route::get('/localization-sections/{position}/{lang}', 'LocalizationController@getSections')->name('get-localization-sections');

	//Permission calls
	Route::get('/permissions', 'PermissionController@getAll')->name('get-permission-items');
	Route::get('/permission/{id_permission}', 'PermissionController@get')->name('get-permission-item');

	//Content calls
	Route::get('/content', 'ContentController@getAll')->name('content');
	Route::get('/content/{id_content}', 'ContentController@get')->name('content-item');
	Route::get('/content-images/{id_content}', 'ContentController@images')->name('get-content-images');

	//TvSchedule calls
	Route::get('/tvschedule-days', 'TvScheduleController@getDays')->name('tvschedule-days');
	Route::get('/tvschedule/{day}/{lang}', 'TvScheduleController@get')->name('tvschedule');

	//Settings calls
	Route::get('/settings', 'SettingsController@getAll')->name('settings');
	Route::get('/settings/{id}', 'SettingsController@get')->name('settings-item');

	//Emails calls
	Route::get('/emails', 'EmailsController@getAll')->name('emails');

	//Gallery
	Route::get('/gallery', 'GalleryController@getAll')->name('emails');

	//Dashboard
	Route::get('/dashboard', 'DashboardController@getData')->name('dashboard');


	//All types calls
	Route::get('/types', 'TypesController@getAll')->name('get-types');
	Route::get('/type/{id_all_type}', 'TypesController@get')->name('get-type');

	Route::get('/ltypes', 'TypesController@getTypes')->name('types');
	
	
});

Route::prefix('add')->group(function(){
	Route::post('/admin', 'AdminController@add')->name('add-admin');
	Route::post('/content', 'ContentController@add')->name('add-content');
	Route::post('/type', 'TypesController@add')->name('add-type');
});

Route::prefix('update')->group(function(){
	Route::post('/profile', 'ProfileController@update')->name('update-profile');
	Route::post('/profile-main-image', 'ProfileController@updateMainImage')->name('update-profile-main-image');
	Route::post('/localization', 'LocalizationController@update')->name('update-localization');
	Route::post('/permission', 'PermissionController@update')->name('update-permissions');
	Route::post('/admin', 'AdminController@update')->name('update-admin');
	Route::post('/admin-main-image', 'AdminController@updateMainImage')->name('update-admin-main-image');

	Route::post('/content', 'ContentController@update')->name('update-content');
	Route::post('/content-main-image', 'ContentController@updateMainImage')->name('update-content-main-image');

	Route::post('/type', 'TypesController@update')->name('update-type');

	Route::post('/tv-schedule', 'TvScheduleController@update')->name('update-tv-schedule');

	Route::post('/settings', 'SettingsController@update')->name('update-settings');
});


Route::prefix('upload')->group(function(){
	//Image calls
	Route::post('/image', 'ImageController@upload')->name('upload');
	Route::post('/broadcast', 'BroadcastController@upload')->name('upload_broadcast');
	Route::post('/profile/image', 'ProfileController@uploadImage')->name('upload-profile-image');
	Route::post('/admin/image', 'AdminController@uploadImage')->name('upload-admin-image');
	Route::post('/content/image', 'ContentController@uploadImage')->name('upload-content-image');
	Route::post('/gallery', 'GalleryController@upload')->name('upload-gallery-image');
});

Route::prefix('delete')->group(function(){
	//Image calls
	Route::post('/image', 'ImageController@deleteImage')->name('delete-image');
	//Broadcast calls
	Route::post('/broadcast', 'BroadcastController@deleteVideo')->name('delete-video');
	//Content calls
	Route::post('/content', 'ContentController@delete')->name('delete-content');
});

Route::prefix('admin')->group(function() {
	Route::get('/', 'AdminController@index')->name('admin');
	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::post('/logout', 'AdminController@adminLogOut')->name('admin.logout');
});

Route::prefix('set')->group(function(){
	Route::post('/locale', 'LanguagesController@setLocale')->name('set-locale');
});

Route::prefix('send')->group(function(){
	Route::post('/email', 'EmailsController@send')->name('send-email');
});