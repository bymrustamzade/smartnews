<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/loadmore-home', 'HomeController@loadMore')->name('loadmore-data');



Route::get('/t/{id_type}', 'TypeController@index')->name('get-type');
Route::get('/loadmore-type/{id_type}', 'TypeController@loadMore')->name('loadmore-type');

Route::get('/v/{slug}', 'ViewController@index')->name('view-content');

Route::get('/p/{topic}', 'TopicController@index')->name('get-topic');
Route::get('/loadmore-topic/{id_topic}', 'TopicController@loadMore')->name('loadmore-topic');

Route::get('/search', 'SearchController@index')->name('search');
Route::get('/loadmore-search', 'SearchController@loadMore')->name('search');

// Route::get('/broadcast', 'BroadcastController@index')->name('broadcast');
// Route::get('/aboutus', 'AppController@aboutus')->name('aboutus');
// Route::get('/tv-schedule', 'TvScheduleController@index')->name('tv-schedule');
// Route::get('/set-locale/{lang}', 'AppController@setLocale')->name('set-locale');
// 
Route::get('/updateapp', function()
{
    \Artisan::call('dump-autoload');
    echo 'dump-autoload complete';
});

Route::get('/get-widget-data', 'SiteController@getWidgetData')->name('widget-data');

Route::get('sitemap.xml', 'SiteController@sitemap')->name('sitemap');